import algoliasearch from 'algoliasearch'
import sanityClient, { SanityDocumentStub } from '@sanity/client'
import indexer, { flattenBlocks } from 'sanity-algolia'

const algolia = algoliasearch("QPLGS1B1N2", "183f1de06a90b9f688d4b0b381e3c2f2");
const sanity = sanityClient({
  projectId: "89obqmi5",
  dataset: "directory",
  // If your dataset is private you need to add a read token.
  // You can mint one at https://manage.sanity.io
  useCdn: false,
});

/**
 *  This function receives webhook POSTs from Sanity and updates, creates or
 *  deletes records in the corresponding Algolia indices.
 */
export async function handler(event) {
  // Tip: Its good practice to include a shared secret in your webhook URLs and
  // validate it before proceeding with webhook handling. Omitted in this short
  // example.
  if (event.queryStringParameters['secret'] !== 'SanitySecret') {
    return {
      statusCode: 400,
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({ message: 'Bad request' }),
    }
  }

  // Configure this to match an existing Algolia index name
  const algoliaIndex = algolia.initIndex('BH_Location')

  const sanityAlgolia = indexer(
    {
      location: {
        index: algoliaIndex,
        projection: `{
          name,
          slug,
          descShort,
          descLong, 
          addr,
          geopoint,
          link,
          logo_alt,
          logo_horizontal {
            asset-> {
              url
            }
          },
          logo_vertical {
            asset-> {
              url
            }
          },
          hrs_247,
          hrs,
          gallery[] {
            alt,
            asset->
          },
          specialty[]->,
          services[] {
            age,
            diagnosis-> {
              name
            },
            levelCare-> {
              name
            },
            serviceLine-> {
              name
            }
          },
          telNum
        }`,
      },
    },
    (document) => {
      if (document._type === "location" && document.services.some(i => i.serviceLine.name.includes("Psychiatry"))=== true) {
        console.log(document)
        document.services ? document.services.map(i=> { if (("age" in i)) { if( i.age.max === undefined ) { i.age.max = 100; } } }) : null
        // Here you will receive 'location' documents, with the fields as
        // prescribed above. So each document here will have `name` and an array
        // of `specialty` documents. That means that if each specialty document
        // has a `title` field for instance, you could index all those titles like
        // this
        return {
          locationName: document.name ? document.name : "",
          slug: document.slug ? document.slug : "",
          descS: document.descShort ? document.descShort : "",
          descL: document.descLong ? document.descLong : "",
          address: document.addr ? document.addr : "",
          _geoloc: document.geopoint ? document.geopoint : "",
          link: document.link ? document.link : "",
          logoA: document.logo_alt ? document.logo_alt : "",
          logoH: document.logo_horizontal ? document.logo_horizontal : "",
          logoV: document.logo_vertical ? document.logo_vertical : "",
          hrs247: document.hrs_247 ? document.hrs_247 : "",
          hrs: document.hrs ? document.hrs : "",
          specialty: document.specialty ? document.specialty.map((s) => s.name) : "",
          gallery: document.gallery ? document.gallery : "",
          services: document.services ? document.services : "",
          telNum: document.telNum ? document.telNum : "",
        };
      }
    }
  );

  // Finally connect the Sanity webhook payload to Algolia indices via the
  // configured serializers and optional visibility function. `webhookSync` will
  // inspect the webhook payload, make queries back to Sanity with the `sanity`
  // client and make sure the algolia indices are synced to match.

  const payload = JSON.parse(event.body)
  return sanityAlgolia.webhookSync(sanity, payload).then(() => {
    return {
      statusCode: 200,
      body: 'ok',
    }
  })
}