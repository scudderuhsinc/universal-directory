import S from "@sanity/desk-tool/structure-builder"
import client from 'part:@sanity/base/client'
import userStore from "part:@sanity/base/user";
import { map } from 'rxjs/operators'
// by vertical
import { RiFolderShield2Line } from "react-icons/ri";
import { RiFolderLine } from "react-icons/ri";
// documents
import { RiHospitalLine } from "react-icons/ri";
import { RiHotelFill } from "react-icons/ri";
import { RiMapPinUserFill } from "react-icons/ri";
import { RiEmpathizeFill } from "react-icons/ri";
import { RiFirstAidKitFill } from "react-icons/ri";
import { RiStethoscopeFill } from "react-icons/ri";
import { RiSyringeFill } from "react-icons/ri";
import { RiAwardFill } from "react-icons/ri";

/*
var contextQuery
if ( process.env.SANITY_STUDIO_CONTEXT === 'gmec' ) {
    //console.log(`document id: `+JSON.stringify( process.env.SANITY_STUDIO_CONTEXT ))
    //console.log(`document id: `+JSON.stringify( process.env.SANITY_STUDIO_CONTEXT_DOC_ID ))
    if ( process.env.SANITY_STUDIO_CONTEXT_DOC_ID ) {
        // GMEC by ID with Providers
        contextQuery = `*[ _type=='gmeConsortium' && _id=='4d5ece91-b4d0-420f-86b4-f7987aa017e3'][0]{
            ...,
            'provider': *[_type=='provider' && references(^._id)]
        }`
        //console.log(`query: `+contextQuery)
    } else {
        // all GMECs with Providers
        contextQuery = `*[_type == 'gmeConsortium']{
            ...,
            'provider': *[_type=='provider' && references(^._id)]
        }`
        //console.log(`query: `+contextQuery)
    }
} else {
    // all records
    contextQuery = `*[]{ ..., }`
    //console.log(`query: `+contextQuery)
}
*/

// https://gist.github.com/runeb/827957cee09d9a83b6882ad6c444f281
// const groupQuery = `*[_type == 'system.group' && identity() in members]{_id}`

//const client = sanityClient.withConfig({ apiVersion: '2021-03-25' })

const sanity = client.withConfig({ apiVersion: '2022-01-01' })

const getRoles = () => 
    userStore.getCurrentUser().then((user) => {
        console.log(`user: `+JSON.stringify(user))
        const { projectId, dataset } = sanity.config();
        return sanity.request({
            uri: `/projects/${projectId}/acl/${user.id}`,
        });
  })
  .then(res => res.roles)

/* export default () => client.fetch(groupQuery) */
  
export default () =>
// Get the current user and their roles
userStore.getCurrentUser().then((user) => user.roles)
  .then(roles => {
        const roleNames = roles.map(r => r.name)
        console.log(`Role Name: `+roleNames)
        const deskItems = []
        if (roleNames.includes('administrator')
            || roleNames.includes('contributor')
            || roleNames.includes('custom')
            || roleNames.includes('developer')
            || roleNames.includes('editor')
            || roleNames.includes('viewer') ) {
            return S.list().title('Content (unfiltered)').items([
            S.documentTypeListItem('provider'),
            S.documentTypeListItem('credential'),
            S.documentTypeListItem('membership'),
            S.documentTypeListItem('role'),
            S.divider(),
            S.listItem()
                .title('Locations')
                .icon(RiHospitalLine)
                .child(
                    S.list()
                    .title('Locations')
                    .items([
                        S.listItem()
                            .title('by Vertical')
                            .icon(RiFolderShield2Line)
                            .child(
                                S.documentList()
                                .title('by Vertical')
                                .menuItems(S.documentTypeList('vertical').getMenuItems())
                                .filter('_type == $type && !defined(parents)')
                                .params({ type: 'vertical' })
                                .child(v =>
                                    S.documentList()
                                    .title('results')
                                    //.title(v) // not working, displays v's _id, want v.name
                                    .menuItems(S.documentTypeList('location').getMenuItems())
                                    .filter('_type == $type && references($v)')
                                    .params({ type: 'location', v })
                                )
                            ),
                            S.listItem()
                            .title('all')
                            .icon(RiFolderLine)
                            .child(
                                S.documentList()
                                .title('all')
                                .filter('_type == $type')
                                .params({ type: 'location' })
                            )
                    ])
                ),
            S.listItem()
                .title('Location Groupings')
                .icon(RiHotelFill)
                .child(
                    S.list()
                    .title('Location Groupings')
                    .items([
                        S.listItem()
                            .title('by Vertical')
                            .icon(RiFolderShield2Line)
                            .child(
                                S.documentList()
                                .title('by Vertical')
                                .menuItems(S.documentTypeList('vertical').getMenuItems())
                                .filter('_type == $type && !defined(parents)')
                                .params({ type: 'vertical' })
                                .child(v =>
                                    S.documentList()
                                    .title('results')
                                    //.title(v) // not working, displays v's _id, want v.name
                                    .menuItems(S.documentTypeList('locationGrp').getMenuItems())
                                    .filter('_type == $type && references($v)')
                                    .params({ type: 'locationGrp', v })
                                )
                            ),
                            S.listItem()
                            .title('all')
                            .icon(RiFolderLine)
                            .child(
                                S.documentList()
                                .title('all')
                                .filter('_type == $type')
                                .params({ type: 'locationGrp' })
                            )
                    ])
                ),
            S.documentTypeListItem('gmeConsortium')
                .title('GME Consortiums')
                .icon(RiMapPinUserFill),
            S.divider(),
            S.listItem()
                .title('Service Lines')
                .icon(RiEmpathizeFill)
                .child(
                    S.list()
                    .title('Service Lines')
                    .items([
                        S.listItem()
                            .title('by Vertical')
                            .icon(RiFolderShield2Line)
                            .child(
                                S.documentList()
                                .title('by Vertical')
                                .menuItems(S.documentTypeList('vertical').getMenuItems())
                                .filter('_type == $type && !defined(parents)')
                                .params({ type: 'vertical' })
                                .child(v =>
                                    S.documentList()
                                    .title('results')
                                    //.title(v) // not working, displays v's _id, want v.name
                                    .menuItems(S.documentTypeList('serviceLine').getMenuItems())
                                    .filter('_type == $type && references($v)')
                                    .params({ type: 'serviceLine', v })
                                )
                            ),
                            S.listItem()
                            .title('all')
                            .icon(RiFolderLine)
                            .child(
                                S.documentList()
                                .title('all')
                                .filter('_type == $type')
                                .params({ type: 'serviceLine' })
                            )
                    ])
                ),
            S.listItem()
                .title('Levels of Care')
                .icon(RiFirstAidKitFill)
                .child(
                    S.list()
                    .title('Levels of Care')
                    .items([
                        S.listItem()
                            .title('by Vertical')
                            .icon(RiFolderShield2Line)
                            .child(
                                S.documentList()
                                .title('by Vertical')
                                .menuItems(S.documentTypeList('vertical').getMenuItems())
                                .filter('_type == $type && !defined(parents)')
                                .params({ type: 'vertical' })
                                .child(v =>
                                    S.documentList()
                                    .title('results')
                                    //.title(v) // not working, displays v's _id, want v.name
                                    .menuItems(S.documentTypeList('levelCare').getMenuItems())
                                    .filter('_type == $type && references($v)')
                                    .params({ type: 'levelCare', v })
                                )
                            ),
                            S.listItem()
                            .title('all')
                            .icon(RiFolderLine)
                            .child(
                                S.documentList()
                                .title('all')
                                .filter('_type == $type')
                                .params({ type: 'levelCare' })
                            )
                    ])
                ),
            S.listItem()
                .title('Diagnoses')
                .icon(RiStethoscopeFill)
                .child(
                    S.list()
                    .title('Diagnoses')
                    .items([
                        S.listItem()
                            .title('by Vertical')
                            .icon(RiFolderShield2Line)
                            .child(
                                S.documentList()
                                .title('by Vertical')
                                .menuItems(S.documentTypeList('vertical').getMenuItems())
                                .filter('_type == $type && !defined(parents)')
                                .params({ type: 'vertical' })
                                .child(v =>
                                    S.documentList()
                                    .title('results')
                                    //.title(v) // not working, displays v's _id, want v.name
                                    .menuItems(S.documentTypeList('diagnosis').getMenuItems())
                                    .filter('_type == $type && references($v)')
                                    .params({ type: 'diagnosis', v })
                                )
                            ),
                            S.listItem()
                            .title('all')
                            .icon(RiFolderLine)
                            .child(
                                S.documentList()
                                .title('all')
                                .filter('_type == $type')
                                .params({ type: 'diagnosis' })
                            )
                    ])
                ),
            S.listItem()
                .title('Treatments')
                .icon(RiSyringeFill)
                .child(
                    S.list()
                    .title('Treatment Services')
                    .items([
                        S.listItem()
                        .title('by Vertical')
                        .icon(RiFolderShield2Line)
                        .child(
                            S.documentList()
                            .title('by Vertical')
                            .menuItems(S.documentTypeList('vertical').getMenuItems())
                            .filter('_type == $type && !defined(parents)')
                            .params({ type: 'vertical' })
                            .child(v =>
                                S.documentList()
                                .title('results')
                                //.title(v) // not working, displays v's _id, want v.name
                                .menuItems(S.documentTypeList('treatment').getMenuItems())
                                .filter('_type == $type && references($v)')
                                .params({ type: 'treatment', v })
                            )
                        ),
                        S.listItem()
                        .title('all')
                        .icon(RiFolderLine)
                        .child(
                            S.documentList()
                            .title('all')
                            .filter('_type == $type')
                            .params({ type: 'treatment' })
                        )
                    ])
                ),
            S.divider(),
            S.listItem()
                .title('Specialty Programs')
                .icon(RiAwardFill)
                .child(
                    S.list()
                    .title('Specialty Programs')
                    .items([
                        S.listItem()
                        .title('by Vertical')
                        .icon(RiFolderShield2Line)
                        .child(
                            S.documentList()
                            .title('by Vertical')
                            .menuItems(S.documentTypeList('vertical').getMenuItems())
                            .filter('_type == $type && !defined(parents)')
                            .params({ type: 'vertical' })
                            .child(v =>
                                S.documentList()
                                .title('results')
                                //.title(v) // not working, displays v's _id, want v.name
                                .menuItems(S.documentTypeList('specialty').getMenuItems())
                                .filter('_type == $type && references($v)')
                                .params({ type: 'specialty', v })
                            )
                        ),
                        S.listItem()
                        .title('all')
                        .icon(RiFolderLine)
                        .child(
                            S.documentList()
                            .title('all')
                            .filter('_type == $type')
                            .params({ type: 'specialty' })
                        )
                    ])
                ),
            S.divider(),
            S.documentTypeListItem('vertical')
            ])
        }
        if (roleNames.includes('gmec')) {
            deskItems.push(
                S.documentTypeListItem('provider')
            )
        }
        if (roleNames.includes('gmec-manager')) {
            deskItems.push(
                S.documentTypeListItem('provider'),
                S.documentTypeListItem('gmeConsortium')
            )
        }
        return S.list().title('Content (filtered)').items(
            deskItems
        )
    })
    .catch(() => {
        return S.list()
            .title('Content')
            .items([
                S.documentTypeListItem('provider'),
                S.documentTypeListItem('credential'),
                S.documentTypeListItem('membership'),
                S.documentTypeListItem('role'),
                S.divider(),
                S.listItem()
                    .title('Locations')
                    .icon(RiHospitalLine)
                    .child(
                        S.list()
                        .title('Locations')
                        .items([
                            S.listItem()
                                .title('by Vertical')
                                .icon(RiFolderShield2Line)
                                .child(
                                    S.documentList()
                                    .title('by Vertical')
                                    .menuItems(S.documentTypeList('vertical').getMenuItems())
                                    .filter('_type == $type && !defined(parents)')
                                    .params({ type: 'vertical' })
                                    .child(v =>
                                        S.documentList()
                                        .title('results')
                                        //.title(v) // not working, displays v's _id, want v.name
                                        .menuItems(S.documentTypeList('location').getMenuItems())
                                        .filter('_type == $type && references($v)')
                                        .params({ type: 'location', v })
                                    )
                                ),
                                S.listItem()
                                .title('all')
                                .icon(RiFolderLine)
                                .child(
                                    S.documentList()
                                    .title('all')
                                    .filter('_type == $type')
                                    .params({ type: 'location' })
                                )
                        ])
                    ),
                S.listItem()
                    .title('Location Groupings')
                    .icon(RiHotelFill)
                    .child(
                        S.list()
                        .title('Location Groupings')
                        .items([
                            S.listItem()
                                .title('by Vertical')
                                .icon(RiFolderShield2Line)
                                .child(
                                    S.documentList()
                                    .title('by Vertical')
                                    .menuItems(S.documentTypeList('vertical').getMenuItems())
                                    .filter('_type == $type && !defined(parents)')
                                    .params({ type: 'vertical' })
                                    .child(v =>
                                        S.documentList()
                                        .title('results')
                                        //.title(v) // not working, displays v's _id, want v.name
                                        .menuItems(S.documentTypeList('locationGrp').getMenuItems())
                                        .filter('_type == $type && references($v)')
                                        .params({ type: 'locationGrp', v })
                                    )
                                ),
                                S.listItem()
                                .title('all')
                                .icon(RiFolderLine)
                                .child(
                                    S.documentList()
                                    .title('all')
                                    .filter('_type == $type')
                                    .params({ type: 'locationGrp' })
                                )
                        ])
                    ),
                S.documentTypeListItem('gmeConsortium')
                    .title('GME Consortiums')
                    .icon(RiMapPinUserFill),
                S.divider(),
                S.listItem()
                    .title('Service Lines')
                    .icon(RiEmpathizeFill)
                    .child(
                        S.list()
                        .title('Service Lines')
                        .items([
                            S.listItem()
                                .title('by Vertical')
                                .icon(RiFolderShield2Line)
                                .child(
                                    S.documentList()
                                    .title('by Vertical')
                                    .menuItems(S.documentTypeList('vertical').getMenuItems())
                                    .filter('_type == $type && !defined(parents)')
                                    .params({ type: 'vertical' })
                                    .child(v =>
                                        S.documentList()
                                        .title('results')
                                        //.title(v) // not working, displays v's _id, want v.name
                                        .menuItems(S.documentTypeList('serviceLine').getMenuItems())
                                        .filter('_type == $type && references($v)')
                                        .params({ type: 'serviceLine', v })
                                    )
                                ),
                                S.listItem()
                                .title('all')
                                .icon(RiFolderLine)
                                .child(
                                    S.documentList()
                                    .title('all')
                                    .filter('_type == $type')
                                    .params({ type: 'serviceLine' })
                                )
                        ])
                    ),
                S.listItem()
                    .title('Levels of Care')
                    .icon(RiFirstAidKitFill)
                    .child(
                        S.list()
                        .title('Levels of Care')
                        .items([
                            S.listItem()
                                .title('by Vertical')
                                .icon(RiFolderShield2Line)
                                .child(
                                    S.documentList()
                                    .title('by Vertical')
                                    .menuItems(S.documentTypeList('vertical').getMenuItems())
                                    .filter('_type == $type && !defined(parents)')
                                    .params({ type: 'vertical' })
                                    .child(v =>
                                        S.documentList()
                                        .title('results')
                                        //.title(v) // not working, displays v's _id, want v.name
                                        .menuItems(S.documentTypeList('levelCare').getMenuItems())
                                        .filter('_type == $type && references($v)')
                                        .params({ type: 'levelCare', v })
                                    )
                                ),
                                S.listItem()
                                .title('all')
                                .icon(RiFolderLine)
                                .child(
                                    S.documentList()
                                    .title('all')
                                    .filter('_type == $type')
                                    .params({ type: 'levelCare' })
                                )
                        ])
                    ),
                S.listItem()
                    .title('Diagnoses')
                    .icon(RiStethoscopeFill)
                    .child(
                        S.list()
                        .title('Diagnoses')
                        .items([
                            S.listItem()
                                .title('by Vertical')
                                .icon(RiFolderShield2Line)
                                .child(
                                    S.documentList()
                                    .title('by Vertical')
                                    .menuItems(S.documentTypeList('vertical').getMenuItems())
                                    .filter('_type == $type && !defined(parents)')
                                    .params({ type: 'vertical' })
                                    .child(v =>
                                        S.documentList()
                                        .title('results')
                                        //.title(v) // not working, displays v's _id, want v.name
                                        .menuItems(S.documentTypeList('diagnosis').getMenuItems())
                                        .filter('_type == $type && references($v)')
                                        .params({ type: 'diagnosis', v })
                                    )
                                ),
                                S.listItem()
                                .title('all')
                                .icon(RiFolderLine)
                                .child(
                                    S.documentList()
                                    .title('all')
                                    .filter('_type == $type')
                                    .params({ type: 'diagnosis' })
                                )
                        ])
                    ),
                S.listItem()
                    .title('Treatments')
                    .icon(RiSyringeFill)
                    .child(
                        S.list()
                        .title('Treatment Services')
                        .items([
                            S.listItem()
                            .title('by Vertical')
                            .icon(RiFolderShield2Line)
                            .child(
                                S.documentList()
                                .title('by Vertical')
                                .menuItems(S.documentTypeList('vertical').getMenuItems())
                                .filter('_type == $type && !defined(parents)')
                                .params({ type: 'vertical' })
                                .child(v =>
                                    S.documentList()
                                    .title('results')
                                    //.title(v) // not working, displays v's _id, want v.name
                                    .menuItems(S.documentTypeList('treatment').getMenuItems())
                                    .filter('_type == $type && references($v)')
                                    .params({ type: 'treatment', v })
                                )
                            ),
                            S.listItem()
                            .title('all')
                            .icon(RiFolderLine)
                            .child(
                                S.documentList()
                                .title('all')
                                .filter('_type == $type')
                                .params({ type: 'treatment' })
                            )
                        ])
                    ),
                S.divider(),
                S.listItem()
                    .title('Specialty Programs')
                    .icon(RiAwardFill)
                    .child(
                        S.list()
                        .title('Specialty Programs')
                        .items([
                            S.listItem()
                            .title('by Vertical')
                            .icon(RiFolderShield2Line)
                            .child(
                                S.documentList()
                                .title('by Vertical')
                                .menuItems(S.documentTypeList('vertical').getMenuItems())
                                .filter('_type == $type && !defined(parents)')
                                .params({ type: 'vertical' })
                                .child(v =>
                                    S.documentList()
                                    .title('results')
                                    //.title(v) // not working, displays v's _id, want v.name
                                    .menuItems(S.documentTypeList('specialty').getMenuItems())
                                    .filter('_type == $type && references($v)')
                                    .params({ type: 'specialty', v })
                                )
                            ),
                            S.listItem()
                            .title('all')
                            .icon(RiFolderLine)
                            .child(
                                S.documentList()
                                .title('all')
                                .filter('_type == $type')
                                .params({ type: 'specialty' })
                            )
                        ])
                    ),
                S.divider(),
                S.documentTypeListItem('vertical')
        ])
    });

/*
export default () =>
    S.list()
    .title('Standard Structure')
    .items([
        S.documentTypeListItem('provider'),
        S.documentTypeListItem('credential'),
        S.documentTypeListItem('membership'),
        S.documentTypeListItem('role'),
        S.divider(),
        S.listItem()
            .title('Locations')
            .icon(RiHospitalLine)
            .child(
                S.list()
                .title('Locations')
                .items([
                    S.listItem()
                        .title('by Vertical')
                        .icon(RiFolderShield2Line)
                        .child(
                            S.documentList()
                            .title('by Vertical')
                            .menuItems(S.documentTypeList('vertical').getMenuItems())
                            .filter('_type == $type && !defined(parents)')
                            .params({ type: 'vertical' })
                            .child(v =>
                                S.documentList()
                                .title('results')
                                //.title(v) // not working, displays v's _id, want v.name
                                .menuItems(S.documentTypeList('location').getMenuItems())
                                .filter('_type == $type && references($v)')
                                .params({ type: 'location', v })
                            )
                        ),
                        S.listItem()
                        .title('all')
                        .icon(RiFolderLine)
                        .child(
                            S.documentList()
                            .title('all')
                            .filter('_type == $type')
                            .params({ type: 'location' })
                        )
                ])
            ),
        S.listItem()
            .title('Location Groupings')
            .icon(RiHotelFill)
            .child(
                S.list()
                .title('Location Groupings')
                .items([
                    S.listItem()
                        .title('by Vertical')
                        .icon(RiFolderShield2Line)
                        .child(
                            S.documentList()
                            .title('by Vertical')
                            .menuItems(S.documentTypeList('vertical').getMenuItems())
                            .filter('_type == $type && !defined(parents)')
                            .params({ type: 'vertical' })
                            .child(v =>
                                S.documentList()
                                .title('results')
                                //.title(v) // not working, displays v's _id, want v.name
                                .menuItems(S.documentTypeList('locationGrp').getMenuItems())
                                .filter('_type == $type && references($v)')
                                .params({ type: 'locationGrp', v })
                            )
                        ),
                        S.listItem()
                        .title('all')
                        .icon(RiFolderLine)
                        .child(
                            S.documentList()
                            .title('all')
                            .filter('_type == $type')
                            .params({ type: 'locationGrp' })
                        )
                ])
            ),
        S.listItem()
            .title('GME Consortiums')
            .icon(RiMapPinUserFill)
            .child(
                S.list()
                .title('GME Consortiums')
                .items([
                    S.listItem()
                        .title('by Vertical')
                        .icon(RiFolderShield2Line)
                        .child(
                            S.documentList()
                            .title('by Vertical')
                            .menuItems(S.documentTypeList('vertical').getMenuItems())
                            .filter('_type == $type && !defined(parents)')
                            .params({ type: 'vertical' })
                            .child(v => 
                                S.documentList()
                                .title('results')
                                //.title(v) // not working, displays v's _id, want v.name
                                .menuItems(S.documentTypeList('gmeConsortium').getMenuItems())
                                .filter('_type == $type && references($v)')
                                .params({ type: 'gmeConsortium', v })
                            )
                        ),
                        S.listItem()
                        .title('all')
                        .icon(RiFolderLine)
                        .child(
                            S.documentList()
                            .title('all')
                            .filter('_type == $type')
                            .params({ type: 'gmeConsortium' })
                        )
                ])
            ),
        S.divider(),
        S.listItem()
            .title('Service Lines')
            .icon(RiEmpathizeFill)
            .child(
                S.list()
                .title('Service Lines')
                .items([
                    S.listItem()
                        .title('by Vertical')
                        .icon(RiFolderShield2Line)
                        .child(
                            S.documentList()
                            .title('by Vertical')
                            .menuItems(S.documentTypeList('vertical').getMenuItems())
                            .filter('_type == $type && !defined(parents)')
                            .params({ type: 'vertical' })
                            .child(v =>
                                S.documentList()
                                .title('results')
                                //.title(v) // not working, displays v's _id, want v.name
                                .menuItems(S.documentTypeList('serviceLine').getMenuItems())
                                .filter('_type == $type && references($v)')
                                .params({ type: 'serviceLine', v })
                            )
                        ),
                        S.listItem()
                        .title('all')
                        .icon(RiFolderLine)
                        .child(
                            S.documentList()
                            .title('all')
                            .filter('_type == $type')
                            .params({ type: 'serviceLine' })
                        )
                ])
            ),
        S.listItem()
            .title('Levels of Care')
            .icon(RiFirstAidKitFill)
            .child(
                S.list()
                .title('Levels of Care')
                .items([
                    S.listItem()
                        .title('by Vertical')
                        .icon(RiFolderShield2Line)
                        .child(
                            S.documentList()
                            .title('by Vertical')
                            .menuItems(S.documentTypeList('vertical').getMenuItems())
                            .filter('_type == $type && !defined(parents)')
                            .params({ type: 'vertical' })
                            .child(v =>
                                S.documentList()
                                .title('results')
                                //.title(v) // not working, displays v's _id, want v.name
                                .menuItems(S.documentTypeList('levelCare').getMenuItems())
                                .filter('_type == $type && references($v)')
                                .params({ type: 'levelCare', v })
                            )
                        ),
                        S.listItem()
                        .title('all')
                        .icon(RiFolderLine)
                        .child(
                            S.documentList()
                            .title('all')
                            .filter('_type == $type')
                            .params({ type: 'levelCare' })
                        )
                ])
            ),
        S.listItem()
            .title('Diagnoses')
            .icon(RiStethoscopeFill)
            .child(
                S.list()
                .title('Diagnoses')
                .items([
                    S.listItem()
                        .title('by Vertical')
                        .icon(RiFolderShield2Line)
                        .child(
                            S.documentList()
                            .title('by Vertical')
                            .menuItems(S.documentTypeList('vertical').getMenuItems())
                            .filter('_type == $type && !defined(parents)')
                            .params({ type: 'vertical' })
                            .child(v =>
                                S.documentList()
                                .title('results')
                                //.title(v) // not working, displays v's _id, want v.name
                                .menuItems(S.documentTypeList('diagnosis').getMenuItems())
                                .filter('_type == $type && references($v)')
                                .params({ type: 'diagnosis', v })
                            )
                        ),
                        S.listItem()
                        .title('all')
                        .icon(RiFolderLine)
                        .child(
                            S.documentList()
                            .title('all')
                            .filter('_type == $type')
                            .params({ type: 'diagnosis' })
                        )
                ])
            ),
        S.listItem()
            .title('Treatments')
            .icon(RiSyringeFill)
            .child(
                S.list()
                .title('Treatment Services')
                .items([
                    S.listItem()
                    .title('by Vertical')
                    .icon(RiFolderShield2Line)
                    .child(
                        S.documentList()
                        .title('by Vertical')
                        .menuItems(S.documentTypeList('vertical').getMenuItems())
                        .filter('_type == $type && !defined(parents)')
                        .params({ type: 'vertical' })
                        .child(v =>
                            S.documentList()
                            .title('results')
                            //.title(v) // not working, displays v's _id, want v.name
                            .menuItems(S.documentTypeList('treatment').getMenuItems())
                            .filter('_type == $type && references($v)')
                            .params({ type: 'treatment', v })
                        )
                    ),
                    S.listItem()
                    .title('all')
                    .icon(RiFolderLine)
                    .child(
                        S.documentList()
                        .title('all')
                        .filter('_type == $type')
                        .params({ type: 'treatment' })
                    )
                ])
            ),
        S.divider(),
        S.listItem()
            .title('Specialty Programs')
            .icon(RiAwardFill)
            .child(
                S.list()
                .title('Specialty Programs')
                .items([
                    S.listItem()
                    .title('by Vertical')
                    .icon(RiFolderShield2Line)
                    .child(
                        S.documentList()
                        .title('by Vertical')
                        .menuItems(S.documentTypeList('vertical').getMenuItems())
                        .filter('_type == $type && !defined(parents)')
                        .params({ type: 'vertical' })
                        .child(v =>
                            S.documentList()
                            .title('results')
                            //.title(v) // not working, displays v's _id, want v.name
                            .menuItems(S.documentTypeList('specialty').getMenuItems())
                            .filter('_type == $type && references($v)')
                            .params({ type: 'specialty', v })
                        )
                    ),
                    S.listItem()
                    .title('all')
                    .icon(RiFolderLine)
                    .child(
                        S.documentList()
                        .title('all')
                        .filter('_type == $type')
                        .params({ type: 'specialty' })
                    )
                ])
            ),
        S.divider(),
        S.documentTypeListItem('vertical')
    ]);
    */