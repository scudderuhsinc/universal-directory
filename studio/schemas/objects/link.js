export default {
    name: 'link',
    type: 'object',
    fields: [
        {
            name: 'home',
            title: 'Home Page',
            type: 'url',
            validation: Rule => Rule.uri({
                scheme: ['http', 'https']
            }).warning(`Define a valid url for this link, beginning with "https://" (best) or "http://".`).required(),
        },
    ],
    preview: {
        select: {
            t: 'home'
        },
        prepare({ t }) {
            const title=`${t}`
            const subtitle=`Home Page`
            return {
                title: title,
                subtitle: subtitle,
            }
        }
    }
}