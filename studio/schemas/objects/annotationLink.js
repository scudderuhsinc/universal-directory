export default {
    name: 'annotationLink',
    title: 'Annotation Link',
    type: 'object',
    fields: [
        {
            name: 'linkText',
            title: 'Link Text',
            type: 'string',
            description: `A string used for a button or html link text, 15 characters or less.`,
            validation: Rule => Rule.error(`You have to define the annotation's link text.`).required(),
            options: {
                maxLength: 15,
            },
        },
        {
            name: 'url',
            title: 'Destination URL',
            type: 'url',
            validation: Rule => Rule.uri({
                scheme: ['http', 'https']
            }).warning(`Define a valid url for this link, beginning with "https://" (best) or "http://".`).required(),
        },
        {
            name: 'type',
            title: 'Type',
            type: 'string',
            description: `Optional, select a type for this link.`,
            options: {
                list: [
                    { title: 'Reference (Internal)', value: 'refInternal' },
                    { title: 'Reference (External)', value: 'refExternal' },
                ],
                layout: 'dropdown'
            },
            validation: Rule => Rule.warning(`Not required but used for automatic formating of design components.`),
        },
    ],
    preview: {
        select: {
            t: 'linkText',
            u: 'url',
        },
        prepare({ t, u }) {
            const title=`${t}`
            const subtitle=`${u}`
            return {
                title: title,
                subtitle: subtitle,
            }
        }
    }
}