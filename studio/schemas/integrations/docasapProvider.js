export default {
    name: 'docasapProvider',
    title: 'DocASAP Provider',
    description: `the DocASAP service, https://docasap.com/.`,
    type: 'object',
    fields: [
        {
            name: 'pSID',
            title: 'Scheduling ID',
            type: 'string'
        },
        {
            name: 'pTC',
            title: 'Tracking Code',
            type: 'slug'
        },
        {
            name: 'suppress',
            title: 'Suppress Service',
            description: `Set to TRUE if the front-end components should be hidden, service details will remain if set.`,
            type: 'boolean'
        },
    ],
    preview: {
        select: {
            i: 'pSID',
            s: 'suppress',
        },
        prepare( {i, s} ) {
            const title=`DocASAP Provider: ${i}`
            const subtitle = `${ Boolean(s) ? 'integration, SUPPRESSED': 'integration, Active'}`
            return {
                title: title,
                subtitle: subtitle,
            }
        }
    },
}
