export default {
    name: 'formLabelInput',
    title: 'Label/Input Pair',
    type: 'object',
    fields: [
        {
            name: 'type',
            type: 'string',
            options: {
                list: [
                    { title: 'Text', value: 'text' },
                    { title: 'Email', value: 'email' },
                    { title: 'Text Area', value: 'textarea' },
                ],
                layout: 'dropdown'
              }
        },
        {
            name: 'label',
            title: 'Label',
            type: 'string',
            description: `Used for form input label and to create input name slug.`,
        },
        {
            name: 'name',
            title: 'Name',
            type: 'slug',
            description: `A URL friendly string, 30 characters or less.`,
            options: {
                maxLength: 30,
            },
        },
        {
            name: 'id',
            title: 'id',
            type: 'slug',
            description: `Optional, A URL friendly string, 15 characters or less.`,
            options: {
                maxLength: 15
            },
        },
        /*
        {
            name: 'help',
            title: 'User Help Text',
            type: 'string',
            description: `Front-end user help text.`,
        },
        */ 
    ],
    preview: {
        select: {
            l: 'label',
            i: 'id.current',
            t: 'type',
            n: 'name.current',
        },
        prepare( {l, i, t, n} ) {
            let title = `undefined`
            let subtitle = `undefined`
            if (t == "textarea"){
                title = `${Boolean(l)? '<label for="'+ n +'">'+ l +'</label>' : 'Label Not Defined'}`
                subtitle = `<textarea name="${n}" ${Boolean(i)? 'id="'+ i +'"' : ''} name="${n}">`
            }
            else {
                title = `${Boolean(l)? '<label for="'+ n +'">'+ l +'</label>' : 'Label Not Defined'}`
                subtitle = `<input type="${t}" ${Boolean(i)? 'id="'+ i +'"' : ''} name="${n}">`
            }
            return {
                title: title,
                subtitle: subtitle,
            }
        }
    },
}