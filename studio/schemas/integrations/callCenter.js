export default {
    name: 'callCenter',
    title: 'Call Center Service Integration',
    type: 'object',
    fieldsets: [
        {
            name: 'disclaimer',
            title: 'Disclaimer',
            options: {
                collapsible: false,
            }
        },
    ],
    options: {
        collapsible: true,
        collapsed: true,
    },
    // used by Heathcare Vertical documents
    fields: [
        {
            name: 'tagLine',
            title: 'Tag Line',
            type: 'string',
            description: `Service description or reporting flag.`
        },
        {
            name: 'telNum',
            title: 'Telephone Number',
            type: 'string',
            description: `Call center telephone number`,
            validation: Rule => Rule.error(`A telephone number is required for this Service Integration.`).required(),
        },
        {
            name: 'disclaimerHeader',
            title: 'Header',
            type: 'string',
            fieldset: 'disclaimer',
            description: `Optional, Call center disclaimer header text.`,
        },
        {
            name: 'disclaimerBody',
            title: 'Body',
            type: 'text',
            rows: 5,
            fieldset: 'disclaimer',
            description: `Call center disclaimer body text.`,
            validation: Rule => Rule.error(`Disclaimer body copy is required for this Service Integration.`).required(),
        },
        /* TODO : optional, disclaimer confirmation action or CTA */
        {
            name: 'form',
            title: 'Form',
            type: 'formCarry',
        },
        /* TODO : Support Chat */
        {
            name: 'suppress',
            title: 'Suppress Service',
            // used for system maintance and updates.
            description: `Set to TRUE if all front-end components should be hidden. All service details will remain if defined.`,
            type: 'boolean'
        },
    ],
    preview: {
        select: {
            tl: 'tagLine',
            tn: 'telNum',
            s: 'suppressed',
        },
        prepare( {tl, tn, s} ) {
            const title = `Call Center: ${tn}`
            const subtitle = `${ Boolean(s) ? '!! SUPPRESSED !!': 'integration Active, '+tl}`
            return {
                title: title,
                subtitle: subtitle,
            }
        }
    },
}