export default {
    name: 'healthLinkNow',
    title: 'Health Link Now Service Integration',
    type: 'object',
    fieldsets: [
        {
            name: 'disclaimer',
            title: 'Disclaimer',
            options: {
                collapsible: false,
            }
        },
    ],
    options: {
        collapsible: true,
        collapsed: true,
    },
    // used by Heathcare Vertical documents
    fields: [
        {
            name: 'tagLine',
            title: 'Tag Line',
            type: 'string',
            description: `Service description or reporting flag.`
        },
        {
            name: 'slug',
            title: 'Slug',
            type: 'slug',
            description: `A URL friendly string, 95 characters or less.`,
            validation: Rule => Rule.error(`You have to define a unique slug shorter than 95 characters.`).required()
        },
        {
            name: 'serviceArea',
            title: 'Service Area(s)',
            type: 'array',
            description: `AddHealth Link Now disclaimer header text.`,
            of: [ { type: 'hlnServiceArea' } ],
        },
        {
            name: 'disclaimerHeader',
            title: 'Header',
            type: 'string',
            fieldset: 'disclaimer',
            description: `Optional, Health Link Now disclaimer header text.`,
        },
        {
            name: 'disclaimerBody',
            title: 'Body',
            type: 'text',
            rows: 5,
            fieldset: 'disclaimer',
            description: `Health Link Now disclaimer body text.`,
            validation: Rule => Rule.error(`Disclaimer body copy is required for this Service Integration.`).required(),
        },
        /* TODO : optional, disclaimer confirmation action or CTA */
        {
            name: 'reqCallback',
            title: 'Request Callback',
            type: 'formCarry',
            description: `Request a Health Link Now callback.`,
        },
        {
            name: 'suppress',
            title: 'Suppress Service',
            // used for system maintance and updates.
            description: `Set to TRUE if all front-end components should be hidden. All service details will remain if defined.`,
            type: 'boolean'
        },
    ],
    preview: {
        select: {
            t: 'tagLine',
            s: 'suppressed',
        },
        prepare( {t, s} ) {
            const title = `Health Link Now`
            const subtitle = `${ Boolean(s) ? '!! SUPPRESSED !!': 'Integration Active, '+t}`
            return {
                title: title,
                subtitle: subtitle,
            }
        }
    },
}