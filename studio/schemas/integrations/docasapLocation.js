export default {
    name: 'docasapLocation',
    title: 'DocASAP Location',
    description: `the DocASAP service, https://docasap.com/.`,
    type: 'object',
    fields: [
        {
            name: 'lSID',
            title: 'Scheduling ID',
            type: 'string'
        },
        {
            name: 'lTC',
            title: 'Tracking Code',
            type: 'slug'
        },
        {
            name: 'suppress',
            title: 'Suppress Service',
            description: `Set to TRUE if the front-end components should be hidden, service details will remain if set.`,
            type: 'boolean'
        },
    ],
    preview: {
        select: {
            i: 'lSID',
            s: 'suppress',
        },
        prepare( {i, s} ) {
            const title = `DocASAP Location: ${i}`
            const subtitle = `${ Boolean(s) ? 'integration, SUPPRESSED': 'integration, Active'}`
            return {
                title: title,
                subtitle: subtitle,
            }
        }
    },
}
