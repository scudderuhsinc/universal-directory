export default {
    name: 'hlnServiceArea',
    title: 'Health Link Now: Service Area',
    type: 'object',
    fields: [
        {
            name: 'geopoint',
            type: 'geopoint'
        },
        {
            name: 'addr',
            title: 'Address',
            type: 'usAddr'
        },
        {
            name: 'telNum',
            title: 'Telephone Number(s)',
            type: 'telNum'
        },
        {
            name: 'slug',
            title: 'Slug',
            type: 'slug',
            description: `A URL friendly string, 95 characters or less.`,
            validation: Rule => Rule.error(`You have to define a unique slug shorter than 95 characters.`).required()
        },
        {
            name: 'serviceLine',
            title: 'Service Line',
            validation: Rule => Rule.error(`You have to define a service line for this treatment service.`).required(),
            weak: true,
            type: 'reference',
            to: [{ type: 'serviceLine'}],
            options: {
                filter: ({document}) => {
                    if (!document._id) {
                        return // all service lines
                    }
                    return {
                        filter: 'references($v)',
                        params: {
                            v: document._id.replace(/^drafts\./, ""),
                        }
                    }
                }
            }
        },
        {
            name: 'levelCare',
            title: 'Level of Care',
            validation: Rule => Rule.error(`You have to define a level of care for this treatment service.`).required(),
            weak: true,
            type: 'reference',
            to: [{  type: 'levelCare' }],
            options: {
                filter: ({document}) => {
                    if (!document._id) {
                        return // all levels of care
                    }
                    return {
                        filter: 'references($v)',
                        params: {
                            v: document._id.replace(/^drafts\./, ""),
                        }
                    }
                }
            }
        },
    ],
    preview: {
        select: {
            t: 'addr.state',
            s: 'telNum.main',
        },
        prepare( {t, s} ) {
            const title = `${t.toUpperCase()} - Service Area`
            const subtitle = `${s}`
            return {
                title: title,
                subtitle: subtitle,
            }
        }
    },
}