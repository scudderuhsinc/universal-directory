export default {
    name: 'formCarry',
    title: 'FormCarry',
    type: 'object',
    options: {
        collapsible: true,
        collapsed: true,
    },
    fields: [
        {
            name: 'actionUrl',
            title: 'Form Action URL',
            type: 'url',
            description: `Form action url, <form action=" -HERE- "> formElem[] </form>`,
            validation: Rule => Rule.uri({
                scheme: ['https']
            }).warning(`A form action source url must be a secure source, https:// .`),
        },
        {
            name: 'formElem',
            title: 'Form Element(s)',
            description: `All form elements, from top to bottom.`,
            type: 'array',
            of: [
                { type: 'formLabelInput' },
                // { type: 'formSubscribe' },
                // { type: 'formCaptcha' },
                // { type: 'formButton' },
            ],
        },
        {
            name: 'scriptUrl',
            title: 'Form Script URL',
            type: 'url',
            description: `Optional, form script source url .. <script src=" -HERE- "></script></body>`,
            validation: Rule => Rule.uri({
                scheme: ['https']
            }).warning(`A form script source url must be a secure source, https:// .`),
        },
        /*
        {
            name: 'class',
            title: 'Form Class(es)',
            description: `All classes for this form's wrapper container. Each class is added individually without a leading period (.).`,
            type: 'array',
            of: [
                { type: 'string' },
            ],
        },
        */
    ]
}