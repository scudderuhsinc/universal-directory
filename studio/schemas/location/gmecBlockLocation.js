export default {
    name: 'gmecBlockLocation',
    title: 'Block, duration & location',
    type: 'object',
    fields: [
        // TODO : Ability to limit Year to `Duration` (year) as definined in Program, gmecProgram.js
        {
            name: 'year',
            title: 'Year',
            type: 'number',
            description: `The year for this curriculum block.`,
            validation: Rule => Rule.error(`You have to define the block year.`).required(),
        },
        {
            name: 'duration',
            title: 'Duration',
            type: 'number',
            description: `The length of this curriculum block in weeks.`,
            validation: Rule => Rule.error(`You have to define the block duration.`).required(),
        },
        {
            name: 'name',
            title: 'Name',
            type: 'string',
            description: `The focus, of this curriculum block.`,
            validation: Rule => Rule.error(`You have to define the block name.`).required(),
        },
        {
            name: 'location',
            title: 'Location',
            description: `The UHS location where this block takes place.`,
            validation: Rule => Rule.error(`You have to define the block location.`).required(),
            weak: true,
            type: 'reference',
            to: {
                type: 'location'
            },
            options: {
                filter: ({document}) => {
                    if (!document.vertical) {
                        return // all healthcare verticals
                    }
                    return {
                        filter: 'references($v)',
                        params: {
                            v: document.vertical._ref
                        }
                    }
                }
            }
        },
        {
            name: 'descShort',
            title: 'Short Description',
            type: 'string',
        }
    ],
    preview: {
        select: {
            n:  'name',
            y:  'year',
            d:  'duration',
            l:  'location.name',
            sd: 'descShort',
        },
        prepare({ n, y, d, l, sd }) {
        let title=`${n}`
        let subtitle=`${y}:${d} - ${l} (yr:wks)`+`${sd? `, ` + sd: ``}`
        return {
            /* media: i, */
            title: title,
            subtitle: subtitle
            }
        }
    }
}