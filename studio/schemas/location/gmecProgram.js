export default {
    name: 'gmecProgram',
    title: 'GMEC Program',
    type: 'object',
    fields: [
        {
            name: 'name',
            title: 'Program Name',
            type: 'string',
            validation: Rule => Rule.error(`You have to define the GMEC Program name.`).required()
        },
        {
            name: 'anchor',
            title: 'Anchor',
            description: `Program specific anchor text.`,
            validation: Rule => Rule.max(60).lowercase().warning(`must be lower case, contain no spaces and be no more than 60 characters.`),
            type: 'string',
        },
        {
            name: 'link',
            title: 'Link(s)',
            type: 'link',
        },
        // TODO : add NRMP Number
        // TODO : add ACGME ID
        // TODO : Link Specialty to Service Line
        /*
        {
            name: 'specialty',
            title: 'Specialty',
            description: `The focus, specialty, of this curriculum block.`,
            validation: Rule => Rule.warning(`it is a good idea to define the block specialty.`),
            weak: true,
            type: 'reference',
            to: {
                type: 'specialty'
            },
            options: {
                filter: ({document}) => {
                    if (!document.vertical) {
                        return // all healthcare verticals
                    }
                    return {
                        filter: 'references($v)',
                        params: {
                            v: document.vertical._ref
                        }
                    }
                }
            }
        },
        */
        {
            name: 'track',
            title: 'Track',
            type: 'string',
            validation: Rule => Rule.warning(`You have to define this program's track.`).required(),
            options: {
                list: [
                    { title: 'Residency', value: 'residency' },
                    { title: 'Fellowship', value: 'fellowship' },
                ],
                layout: 'dropdown'
              }
        },
        {
            name: 'duration',
            title: 'Duration',
            type: 'number',
            description: `The overall length of this program in years. Each year starts 7/1 and ends 6/30.`,
            validation: Rule => Rule.warning(`You have to define this program's duration in years, e.g. "PGY-2" is a 2 year program.`).required(),
            options: {
                list: [ 1, 2, 3, 4, 5, 6 ],
                layout: 'dropdown'
              }
        },
        {
            name: 'block',
            title: 'Block(s)',
            description: `All Curriculum Blocks for this program.`,
            type: 'array',
            validation: Rule => Rule.warning(`You have to define at least one block for this program.`).required(),
            // TODO : Require total number of Block weeks == | !> Duration, in weeks
            // TODO : Ability to Group Blocks into Academic Years
            of: [
                { type: 'gmecBlockLocation' }, // block with a UHS Location and Duration
                { type: 'gmecBlock' } // block, Duration Only
            ]  
        },
        // Always One Provider 
        {
            name: 'director',
            title: 'Program Director',
            weak: true,
            description: `Update individual Providers first; each Provider MUST be defined as having a 'Faculty' Role with this GMEC before defined as a Program Director.`,
            type: 'reference',
            to: [{  type: 'provider' }],
            options: {
                // TODO : limit to Providers with Faculty Role for this GMEC
                filter: 'roleTypeGmec.type == $rt',
                filterParams: { rt: "faculty" }
            },
        },
        // Always Provider(s)
        {
            name: 'directorAssoc',
            title: 'Associate Program Director',
            weak: true,
            description: `Each Provider MUST be defined as having a 'Faculty' Role with this GMEC before defined as a Associate Program Director.`,
            type: 'array',
            of: [ {
                    type: 'reference',
                    weak: true,
                    to: {
                        type: 'provider'
                    },
                options: {
                    filter: ({document}) => {
                    // TODO : limit to Providers with Faculty Role for this GMEC and NOT the Program Director
                    // console.log(`here `+JSON.stringify(document));
                    // console.log(`here `+JSON.stringify(document.program[0].director));
                    if (!document._id) {
                        return // all providers
                    }
                    return {
                            filter: 'roleTypeGmec.gmec._ref == $id && roleTypeGmec.type == $rt',
                            params: {
                                id: document._id.replace(/^drafts\./, ""),
                                rt: "faculty",
                                //dr: document.program.director,
                            }
                        }
                    }
                }
            } ]                      
        },
        // Always Provider(s)
        {
            name: 'coreFaculty',
            title: 'Core Faculty',
            description: `Each Provider MUST be defined as having a 'Faculty' Role with this GMEC before defined as Core Faculty.`,
            type: 'array',
            of: [ {
                    type: 'reference',
                    weak: true,
                    to: {
                        type: 'provider'
                    },
                options: {
                    // TODO : remove Director and Associate Director from list of Provider options.
                    filter: ({document}) => {
                        if (!document._id) {
                            return // all providers
                        }
                        return {
                            filter: 'references($id) && roleTypeGmec.type == $rt',
                            params: {
                                id: document._id.replace(/^drafts\./, ""),
                                rt: "faculty",
                                //dr: document.program.director,
                                //da: document.program.directorAssociate,
                            }
                        }

                    }
                }
            } ]
        }
        // A Current Trainee or Program Graduate is defined within a Provider record.
    ],
    preview: {
        select: {
            n: 'name',
            //s: 'specialty.name',
            t: 'track',
            d: 'duration'
        },
        prepare({ n, s, t, d }) {
            let title=`${n}`
            let subtitle=`${ t.charAt(0).toUpperCase() + t.slice(1) }${ d? `, `+ d + ` Year Track`: ` Track`}`
            return {
                title: title,
                subtitle: subtitle
            }
        }
    }
}