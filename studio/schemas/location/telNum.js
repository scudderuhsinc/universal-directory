export default {
    name: 'telNum',
    type: 'object',
    fields: [
        {
            name: 'main',
            type: 'string',
        },
        {
            name: 'appt',
            title: 'For Appointments',
            type: 'string',
        },
        {
            name: 'fax',
            type: 'string',
        },
    ]
}