export default {
    name: 'close',
    type: 'object',
    fieldsets: [
        {
            name: 'time',
            options: {
                collapsible: false,
                columns: 2
            },
        }
    ],
    fields: [
        {
            name: 'hour',
            type: 'string',
            options: {
                list: [
                    { title: '12 noon', value: '12' },
                    { title: ' 1 pm', value: '13' },
                    { title: ' 2 pm', value: '14' },
                    { title: ' 3 pm', value: '15' },
                    { title: ' 4 pm', value: '16' },
                    { title: ' 5 pm', value: '17' },
                    { title: ' 6 pm', value: '18' },
                    { title: ' 7 pm', value: '19' },
                    { title: ' 8 pm', value: '20' },
                    { title: ' 9 pm', value: '21' },
                    { title: '10 pm', value: '22' },
                    { title: '11 pm', value: '23' },
                    { title: '12 midnight', value: '00' },
                    { title: ' 1 am', value: '01' },
                    { title: ' 2 am', value: '02' },
                    { title: ' 3 am', value: '03' },
                    { title: ' 4 am', value: '04' },
                    { title: ' 5 am', value: '05' },
                    { title: ' 6 am', value: '06' },
                    { title: ' 7 am', value: '07' },
                    { title: ' 8 am', value: '08' },
                    { title: ' 9 am', value: '09' },
                    { title: '10 am', value: '10' },
                    { title: '11 am', value: '11' },
                ],
                layout: 'dropdown',
            },
            fieldset: 'time',
        },
        {
            name: 'minute',
            type: 'string',
            options: {
                list: [
                    { title: '00', value: '00' },
                    { title: '15', value: '15' },
                    { title: '30', value: '30' },
                    { title: '45', value: '45' },
                ],
                layout: 'dropdown',
            },
            fieldset: 'time',
        },
    ],
    preview: {
        select: {
            hr:  'hour',
            mn:  'minute',
        },
        prepare({ hr, mn }) {
            const title = `${hr}:${mn}`
            return {
                title: title,
            }
        }
    }
}
        