export default {
    name: 'hrs',
    type: 'object',
    fieldsets: [
        {
            name: 'monday',
            title: 'Monday',
            options: {
                collapsible: false,
                columns: 2
            }
        },
        {
            name: 'tuesday',
            title: 'Tuesday',
            options: {
                collapsible: false,
                columns: 2
            }
        },
        {
            name: 'wednesday',
            title: 'Wednesday',
            options: {
                collapsible: false,
                columns: 2
            }
        },
        {
            name: 'thursday',
            title: 'Thursday',
            options: {
                collapsible: false,
                columns: 2
            }
        },
        {
            name: 'friday',
            title: 'Friday',
            options: {
                collapsible: false,
                columns: 2
            }
        },
        {
            name: 'saturday',
            title: 'Saturday',
            options: {
                collapsible: false,
                columns: 2
            }
        },
        {
            name: 'sunday',
            title: 'Sunday',
            options: {
                collapsible: false,
                columns: 2,
            }
        },
    ],
    fields: [
        {
            name: 'mondayOpen',
            title: 'Open',
            type: 'open',
            fieldset: 'monday',
            options: {
                collapsible: false,
            }
        },
        {
            name: 'mondayClose',
            title: 'Close',
            type: 'close',
            fieldset: 'monday',
            options: {
                collapsible: false,
            }
        },
        {
            name: 'tuesdayOpen',
            title: 'Open',
            type: 'open',
            fieldset: 'tuesday',
            options: {
                collapsible: false,
            }
        },
        {
            name: 'tuesdayClose',
            title: 'Close',
            type: 'close',
            fieldset: 'tuesday',
            options: {
                collapsible: false,
            }
        },
        {
            name: 'wednesdayOpen',
            title: 'Open',
            type: 'open',
            fieldset: 'wednesday',
            options: {
                collapsible: false,
            }
        },
        {
            name: 'wednesdayClose',
            title: 'Close',
            type: 'close',
            fieldset: 'wednesday',
            options: {
                collapsible: false,
            }
        },
        {
            name: 'thursdayOpen',
            title: 'Open',
            type: 'open',
            fieldset: 'thursday',
            options: {
                collapsible: false,
            }
        },
        {
            name: 'thursdayClose',
            title: 'Close',
            type: 'close',
            fieldset: 'thursday',
            options: {
                collapsible: false,
            }
        },
        {
            name: 'fridayOpen',
            title: 'Open',
            type: 'open',
            fieldset: 'friday',
            options: {
                collapsible: false,
            }
        },
        {
            name: 'fridayClose',
            title: 'Close',
            type: 'close',
            fieldset: 'friday',
            options: {
                collapsible: false,
            }
        },
        {
            name: 'saturdayOpen',
            title: 'Open',
            type: 'open',
            fieldset: 'saturday',
            options: {
                collapsible: false,
            }
        },
        {
            name: 'saturdayClose',
            title: 'Close',
            type: 'close',
            fieldset: 'saturday',
            options: {
                collapsible: false,
            }
        },
        {
            name: 'sundayOpen',
            title: 'Open',
            type: 'open',
            fieldset: 'sunday',
            options: {
                collapsible: false,
            }
        },
        {
            name: 'sundayClose',
            title: 'Close',
            type: 'close',
            fieldset: 'sunday',
            options: {
                collapsible: false,
            }
        },
    ],
}