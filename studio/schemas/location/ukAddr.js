export default {
    name: 'ukAddr',
    title: 'United Kingdom Address',
    type: 'object',
    fields: [
        {
            name: 'line1',
            type: 'string',
            description: `House/building name -or- house number and the street name.`
        },
        {
            name: 'line2',
            type: 'string',
            description: `Optional, if the house/building has a name, add the remaining street details.`
        },
        {
            name: 'locality',
            type: 'string',
            description: `Optional, locality name.`,
            // https://www.sanity.io/schemas/country-dropdown-list-cd95c6d6
            options: {
                list: [
                    {title: 'Birmingham', value: 'Birmingham'},
                    {title: 'Liverpool', value: 'Liverpool'},
                    {title: 'Leeds', value: 'Leeds'},
                    {title: 'Sheffield', value: 'Sheffield'},
                    {title: 'Bristol', value: 'Bristol'},
                    {title: 'Manchester', value: 'Manchester'},
                    {title: 'Leicester', value: 'Leicester'},
                    {title: 'Coventry', value: 'Coventry'},
                    {title: 'Croydon', value: 'Croydon'},
                    {title: 'Barnet', value: 'Barnet'},
                    {title: 'Kingston upon Hull', value: 'Kingston upon Hull'},
                    {title: 'Ealing', value: 'Ealing'},
                    {title: 'Bradford', value: 'Bradford'},
                    {title: 'Bromley', value: 'Bromley'},
                    {title: 'Enfield', value: 'Enfield'},
                    {title: 'Lambeth', value: 'Lambeth'},
                    {title: 'Brent', value: 'Brent'},
                    {title: 'Wandsworth', value: 'Wandsworth'},
                    {title: 'Stroke-on-Trent', value: 'Stroke-on-Trent'},
                    {title: 'Wolverhampton', value: 'Wolverhampton'},
                    {title: 'more', value: 'more'}, //https://en.wikipedia.org/wiki/List_of_cities_in_the_United_Kingdom
                ]
            }
        },
        {
            name: 'city',
            type: 'string',
            description: `City or town name, in all capitals.`,
        },
        {
            name: 'state',
            type: 'string',
            description: `State or province name.`,
            options: {
                list: [
                    {title: 'Aberconwy and Colwyn', value: 'I0'},
                    {title: 'Aberdeen City', value: 'I1'},
                    {title: 'Aberdeenshire', value: 'I2'},
                    {title: 'Anglesey', value: 'I3'},
                    {title: 'Angus', value: 'I4'},
                    {title: 'Antrim', value: 'I5'},
                    {title: 'Argyll and Bute', value: 'I6'},
                    {title: 'Armagh', value: 'I7'},
                    {title: 'Avon', value: 'I8'},
                    {title: 'Ayrshire', value: 'I9'},
                    {title: 'Bath and NE Somerset', value: 'IB'},
                    {title: 'Bedfordshire', value: 'IC'},
                    {title: 'Belfast', value: 'IE'},
                    {title: 'Berkshire', value: 'IF'},
                    {title: 'Berwickshire', value: 'IG'},
                    {title: 'BFPO', value: 'IH'},
                    {title: 'Blaenau Gwent', value: 'II'},
                    {title: 'Buckinghamshire', value: 'IJ'},
                    {title: 'Caernarfonshire', value: 'IK'},
                    {title: 'Caerphilly', value: 'IM'},
                    {title: 'Caithness', value: 'IO'},
                    {title: 'Cambridgeshire', value: 'IP'},
                    {title: 'Cardiff', value: 'IQ'},
                    {title: 'Cardiganshire', value: 'IR'},
                    {title: 'Carmarthenshire', value: 'IS'},
                    {title: 'Ceredigion', value: 'IT'},
                    {title: 'Channel Islands', value: 'IU'},
                    {title: 'Cheshire', value: 'IV'},
                    {title: 'City of Bristol', value: 'IW'},
                    {title: 'Clackmannanshire', value: 'IX'},
                    {title: 'Clwyd', value: 'IY'},
                    {title: 'Conwy', value: 'IZ'},
                    {title: 'Cornwall/Scilly', value: 'J0'},
                    {title: 'Cumbria', value: 'J1'},
                    {title: 'Denbighshire', value: 'J2'},
                    {title: 'Derbyshire', value: 'J3'},
                    {title: 'Derry/Londonderry', value: 'J4'},
                    {title: 'Devon', value: 'J5'},
                    {title: 'Dorset', value: 'J6'},
                    {title: 'Down', value: 'J7'},
                    {title: 'Dumfries and Galloway', value: 'J8'},
                    {title: 'Dunbartonshire', value: 'J9'},
                    {title: 'Dundee', value: 'JA'},
                    {title: 'Durham', value: 'JB'},
                    {title: 'Dyfed', value: 'JC'},
                    {title: 'more', value: 'more'},
                ],
                layout: 'dropdown'
              }
        },
        {
            name: 'department',
            type: 'string',
            description: `Optional, department name.`
        },
        {
            name: 'code',
            type: 'string',
            description: `postal code`,
        },
    ],
    preview: {
        select: {
            l1: 'line1',
            l2: 'line2',
            lc: 'locality',
            t: 'city',
            s: 'state',
            c: 'code'
        },
        prepare({ l1, l2, lc, t, s, c }) {
            const title=`${l1}` + `${Boolean(l2)? ', '+l2 : ''}`
            const subtitle=`${Boolean(lc)? lc +', ' : ''}`+`${t}, ${s} ${c}`
            return {
                title: title,
                subtitle: subtitle,
            }
        }
    }
}