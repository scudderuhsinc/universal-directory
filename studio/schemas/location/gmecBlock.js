export default {
    name: 'gmecBlock',
    title: 'Block, duration only',
    type: 'object',
    fields: [
        // TODO : Ability to limit Year to `Duration` (year) as definined in Program, gmecProgram.js
        {
            name: 'year',
            title: 'Year',
            type: 'number',
            description: `The year for this curriculum block.`,
            validation: Rule => Rule.error(`You have to define the block year.`).required(),
        },
        {
            name: 'duration',
            title: 'Duration',
            type: 'number',
            description: `The length of this curriculum block in weeks.`,
            validation: Rule => Rule.error(`You have to define the block duration.`).required(),
        },
        {
            name: 'name',
            title: 'Name',
            type: 'string',
            description: `The focus, of this curriculum block.`,
            validation: Rule => Rule.error(`You have to define the block name.`).required(),
        },
        {
            name: 'location',
            title: 'Location (alt)',
            type: 'string',
            description: `The non-Standard or non-UHS facility where this block takes place.`,
            validation: Rule => Rule.error(`You have to define the block location.`).required(),

        },
        {
            name: 'descShort',
            title: 'Short Description',
            type: 'string',
        }
    ],
    preview: {
        select: {
            n:  'name',
            y:  'year',
            d:  'duration',
            l:  'location',
            sd: 'descShort',
        },
        prepare({ n, y, d, l, sd }) {
        let title=`${n}`
        let subtitle=`${y}:${d} - ${l} (yr:wks)`+`${sd? `, ` + sd: ``}`
        return {
            /* media: i, */
            title: title,
            subtitle: subtitle
            }
        }
    }
}