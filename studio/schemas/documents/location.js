import { RiHospitalLine } from "react-icons/ri";

export default {
    name: 'location',
    title: 'Locations',
    type: 'document',
    icon: RiHospitalLine,
    fieldsets: [
        {
            name: 'descriptions',
            title: 'Description(s)',
            description: `Optional, location short and long descriptions.`,
            options: {
                collapsible: true,
                collapsed: true,
            }
        },
        {
            name: 'hours',
            title: 'Hours',
            options: {
                collapsible: false,
            }
        },
        {
            name: 'assets',
            title: 'Asset(s)',
            options: {
                collapsible: true,
                collapsed: true,
            }
        },
        {
            name: 'gallery',
            title: 'Gallery',
            options: {
                collapsible: true,
                collapsed: true,
            }
        },
    ],
    fields: [
        {
            name: 'name',
            title: 'Name',
            type: 'string',
            validation: Rule => Rule.error(`You have to define the location's name.`).required()
        },
        {
            name: 'slug',
            title: 'Slug',
            type: 'slug',
            validation: Rule => Rule.error(`You have to define a unique slug shorter than 95 characters.`).required(),
            description: `A URL friendly string, 95 characters or less.`,
            options: {
                source: doc => doc.name,
                maxLength: 95,
                slugify: input => input
                    .toLowerCase()
                    .replace(/[&\/\\#,+()$~%.'":*?<>{}]/g, '')
                    .replace(/ - /g, '-')
                    .replace(/\s+/g, '-')
                    .slice(0, 95)
            },
        },
        {
            name: 'vertical',
            title: 'Healthcare Vertical',
            description: `The location's healthcare vertical.`,
            validation: Rule => Rule.error(`You have to define the location's healthcare vertical.`).required(),
            type: 'reference',
            to: {
                weak: true,
                type: 'vertical'
            }
        },
        {
            name: 'anchor',
            title: 'Anchor',
            description: `Location specific anchor text.`,
            validation: Rule => Rule.max(60).lowercase().warning(`must be lower case, contain no spaces and be no more than 60 characters.`),
            type: 'string',
        },
        {
            name: 'descShort',
            title: 'short',
            type: 'string',
            fieldset: 'descriptions',
        },
        {
            name: 'descLong',
            title: 'long',
            type: 'locationPortableText',
            fieldset: 'descriptions',
        },
        {
            name: 'geopoint',
            type: 'geopoint'
        },
        {
            name: 'addr',
            title: 'Address',
            type: 'usAddr'
        },
        {
            name: 'telNum',
            title: 'Telephone Number(s)',
            type: 'telNum'
        },
        {
            name: 'link',
            title: 'Link(s)',
            type: 'link',
        },
        /* TODO : Social Links */
        {
            name: 'hrs_247',
            title: 'Open 24/7',
            type: 'boolean',
            fieldset: 'hours'
        },
        {
            name: 'hrs',
            title: 'Daily Hours',
            type: 'hrs',
            fieldset: 'hours',
            options: {
                collapsible: true,
                collapsed: true,
            }
        },
        {
            name: 'services',
            title: 'Treatment Service(s)',
            description: `How a service is billed, clinical description.`,
            type: 'array',
            of: [
                { type: 'treatmentService' },
            ],
        },
        {
            name: 'specialty',
            title: 'Specialty Program(s)',
            description: `How a service is marketed, colloquial description.`,
            type: 'array',
            of: [
                { type: 'reference',
                    to: {
                        weak: true,
                        type: 'specialty',
                    },
                },
            ],
        },
        {
            name: 'logo_horizontal',
            title: 'logo (horizontal)',
            type: 'image',
            fieldset: 'assets',
            options: {
                hotspot: true,
                /* accept: 'image/svg+xml', */
            },
        },
        {
            name: 'logo_vertical',
            title: 'logo (vertical)',
            type: 'image',
            fieldset: 'assets',
            options: {
                hotspot: true,
                /* accept: 'image/svg+xml', */
            },
        },
        {
            name: 'logo_square',
            title: 'logo (square)',
            type: 'image',
            fieldset: 'assets',
            options: {
                hotspot: true,
                /* accept: 'image/svg+xml', */
            },
        },
        {
            name: 'logo_alt',
            title: 'logo alternative text',
            type: 'string',
            fieldset: 'assets',
            description: 'Important for SEO and accessiblity.',
            validation: Rule => Rule.warning('For accessiblity define alternative text for logo(s).'),
            options: {
                isHighlighted: true
            }
        },
        {
            name: 'gallery',
            title: 'Image(s)',
            type: 'array',
            of: [
                { type: 'galleryImage' },
            ],
            fieldset: 'gallery',
        },
    ],
    orderings: [
        {
            name: 'nameAsc',
            title: 'Location Name a–>z',
            by: [{
                    field: 'name',
                    direction: 'asc'
            }]
        },
        {
            name: 'nameDesc',
            title: 'Location Name z->a',
            by: [{
                    field: 'name',
                    direction: 'desc'
            }]
        }
    ],
    preview: {
        select: {
            t: 'name',
            s: 'descShort',
            i: 'logo_horizontal.asset.url',
        },
        prepare({ t, s, i }) {
            const title=`${t}`
            const subtitle=`${s}`
            return {
                imageUrl: i,
                title: title,
                subtitle: subtitle,
            }
        }
    }
}
