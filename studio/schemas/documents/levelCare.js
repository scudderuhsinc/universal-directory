import { RiFirstAidKitFill } from "react-icons/ri";

export default {
    name: 'levelCare',
    title: 'Levels of Care',
    type: 'document',
    icon: RiFirstAidKitFill,
    fieldsets: [
        {
            name: 'descriptions',
            title: 'Description(s)',
            description: `Optional, level of care short and long descriptions.`,
            options: {
                collapsible: true,
                collapsed: true,
            }
        },
    ],
    fields: [
        {
            name: 'name',
            title: 'Name',
            type: 'string',
            description: `Service line name`
        },
        {
            name: 'vertical',
            title: 'Healthcare Vertical',
            type: 'reference',
            validation: Rule => Rule.error(`You have to define the level of care's healthcare vertical.`).required(),
            to: {
                weak: true,
                type: 'vertical'
            }
        },
        {
            name: 'descShort',
            title: 'short',
            type: 'string',
            fieldset: 'descriptions',
        },
        {
            name: 'descLong',
            title: 'long',
            type: 'specialtyPortableText',
            fieldset: 'descriptions',
        },
    ],
    orderings: [
        {
            name: 'nameAsc',
            title: 'Location Name a–>z',
            by: [{
                    field: 'name',
                    direction: 'asc'
            }]
        },
        {
            name: 'nameDesc',
            title: 'Location Name z->a',
            by: [{
                    field: 'name',
                    direction: 'desc'
            }]
        }
    ],
    preview: {
        select: {
            t: 'name',
            s: 'descShort',
        },
        prepare({ t, s }) {
            const title = `${t}`
            const subtitle = `${s}`
            return {
                title: title,
                subtitle: subtitle
            }
        }
    }
}