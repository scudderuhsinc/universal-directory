import { RiHotelFill } from "react-icons/ri";

export default {
    name: 'locationGrp',
    title: 'Location Groupings',
    type: 'document',
    icon: RiHotelFill,
    fields: [
        {
            name: 'name',
            title: 'Name',
            type: 'string',
            validation: Rule => Rule.error(`You have to define the group's name.`).required()
        },
        /*
        {
            name: 'slug',
            title: 'Slug',
            description: `A URL friendly string, 95 characters or less.`,
            type: 'slug',
            options: {
                source: doc => doc.name,
                maxLength: 95,
                slugify: input => input
                    .toLowerCase()
                    .replace(/\s+/g, '-')
                    .slice(0, 95)
            },
        },
        */
        {
            name: 'vertical',
            title: 'Healthcare Vertical',
            type: 'reference',
            to: {
                weak: true,
                type: 'vertical'
            }
        },
        {
            name: 'anchor',
            title: 'Anchor',
            description: `Location group specific anchor text.`,
            validation: Rule => Rule.max(60).lowercase().warning(`must be lower case, contain no spaces and be no more than 60 characters.`),
            type: 'string',
        },
        {
            name: 'link',
            title: 'Link(s)',
            description: `Location group specific links, leave blank if a duplicate of the keystone's links.`,
            type: 'link',
        },
        /*  TODO : Social Links */
        /*
            TODO : Refine relationships of Locations
            // https://www.sanity.io/guides/deciding-fields-and-relationships
            // https://www.sanity.io/guides/hierarchies-graphs-navigation
        */
        {
            name: 'keystone',
            title: 'Keystone Location',
            description: `Optional, a single keystone location (top) or location group (bottom of listing).`,
            weak: true,
            type: 'reference',
            to: [
                {type: 'location'},
                {type: 'locationGrp'}
            ],
            options: {
                filter: ({document}) => {
                    if (!document.vertical) {
                        return // all healthcare verticals
                    }
                    return {
                        filter: 'references($v) && $id != _id',
                        params: {
                            v: document.vertical._ref,
                            id: document._id.replace(/^drafts\./, "")
                        }
                    }
                }
            }
        },
        {
            name: 'location',
            title: 'Location(s)',
            description: `All individual locations for this group, except (not) the keystone location.`,
            type: 'array',
            of: [ {
                weak: true,
                type: 'reference',
                to: {
                    type: 'location'
                },
                options: {
                filter: ({document}) => {
                    if (!document.vertical) {
                        return // all healthcare verticals
                    }
                    return {
                        filter: 'references($v)',
                        params: {
                            v: document.vertical._ref
                        }
                    }
                }
            }
            } ],
        },
        {
            name: 'alignGrp',
            title: 'Aligned Groupings',
            description: `Any location groupings, aligned with this group's vertical.`,
            type: 'array',
            of: [ {
                weak: true,
                type: 'reference',
                to: {
                    type: 'locationGrp'
                },
                options: {
                filter: ({document}) => {
                    console.log("here: "+document._id);
                    if (!document.vertical) {
                        return // all healthcare verticals
                    }
                    return {
                        filter: 'references($v) && $id != _id',
                        params: {
                            v: document.vertical._ref,
                            id: document._id.replace(/^drafts\./, "")
                        }
                    }
                }
            }
            } ],
        },
        {
            name: 'parallelGrp',
            title: 'Parallel Groupings',
            description: `Any location groupings, that are NOT aligned under this group's vertical.`,
            type: 'array',
            of: [ {
                weak: true,
                type: 'reference',
                to: {
                    type: 'locationGrp'
                },
                options: {
                filter: ({document}) => {
                    if (!document.vertical) {
                        return // all healthcare verticals
                    }
                    return {
                        filter: '!references($v)',
                        params: {
                            v: document.vertical._ref
                        }
                    }
                }
            }
            } ],
        },
        {
            name: 'gmec',
            title: 'Graduate Medical Education Consortium',
            description: `A single GMEC associated with this location grouping, additional GMECs should be added individually to a Keystone Location, Aligned or Parallel Grouping child.`,
            weak: true,
            type: 'reference',
            to: {
                type: 'gmeConsortium'
            },
        },
    ],
    orderings: [
        {
            name: 'nameAsc',
            title: 'Group Name a–>z',
            by: [
                {
                    field: 'name',
                    direction: 'asc'
                }
            ]
        },
        {
            name: 'nameDesc',
            title: 'Group Name z->a',
            by: [
                {
                    field: 'name',
                    direction: 'desc'
                }
            ]
        }
    ],
    preview: {
        select: {
            n: 'name'
        },
        prepare({ n }) {
            let title=`${n}`
            //let subtitle=`not set`
            return {
                /* media: i, */
                title: title,
                /* subtitle: subtitle */
            }
        }
    }
}