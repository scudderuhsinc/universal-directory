import { RiBankFill } from "react-icons/ri";

export default {
    name: 'credential',
    title: 'Credentials',
    type: 'document',
    icon: RiBankFill,
    fields: [
        {
            name: 'init',
            title: 'Initials',
            type: 'string'
        },
        {
            name: 'descShort',
            title: 'Short Description',
            type: 'string',
        },
    ],
    orderings: [
        {
            name: 'initAsc',
            title: 'Credential a–>z',
            by: [
                {
                    field: 'init',
                    direction: 'asc'
                }
            ]
        },
        {
            name: 'initDesc',
            title: 'Credential z->a',
            by: [
                {
                    field: 'init',
                    direction: 'desc'
                }
            ]
        }
    ],
    preview: {
        select: {
            i: 'init',
            d: 'descShort',
        },
        prepare({ i, d }) {
            const t=`${i}`
            const st=`${d}`
            return {
                title: t,
                subtitle: st,
            }
        }
    }
}