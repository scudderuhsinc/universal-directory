import { RiMapPinUserFill } from "react-icons/ri";

export default {
    name: 'gmeConsortium',
    title: 'GME Consortiums',
    type: 'document',
    icon: RiMapPinUserFill,
    fields: [
        {
            name: 'name',
            title: 'Name',
            type: 'string',
            description:
                `Graduate medical education consortium name.`,
            type: 'string',
            validation: Rule => Rule.error(`You have to define the Graduate Medical Education Consortium's name.`).required(),
            hidden: ({currentUser}) => {
                // can not edit
                return (currentUser.roles.find(({name}) => name === 'gmec' ))
            }
        },
        {
            name: 'anchor',
            title: 'Anchor',
            type: 'string',
            description: 
                `Program specific anchor text.`,
            validation: Rule => Rule.max(60).lowercase().warning(`must be lower case, contain no spaces and be no more than 60 characters.`),
            hidden: ({currentUser}) => {
                // can not edit
                return (currentUser.roles.find(({name}) => name === 'gmec' || name === 'gmec-manager')) 
            }
        },
        /*
        {
            name: 'slug',
            title: 'Slug',
            description: `A URL friendly string, 95 characters or less.`,
            type: 'slug',
            options: {
                source: doc => doc.name,
                maxLength: 95,
                slugify: input => input
                    .toLowerCase()
                    .replace(/\s+/g, '-')
                    .slice(0, 95)
            },
        },
        */
        {
            name: 'vertical',
            title: 'Healthcare Vertical',
            type: 'reference',
            to: {
                weak: true,
                type: 'vertical'
            },
            hidden: ({currentUser}) => {
                // can not edit
                return (currentUser.roles.find(({name}) => name === 'gmec' || name === 'gmec-manager')) 
            }
        },
        {
            name: 'keystone',
            title: 'Keystone Location',
            description: `Optional, a single keystone location (top) or location group (bottom of listing).`,
            weak: true,
            type: 'reference',
            to: [
                {type: 'location'},
                {type: 'locationGrp'}
            ],
        },
        {
            name: 'dio',
            title: 'Designated Institutional Official',
            weak: true,
            type: 'reference',
            to: [{  type: 'provider' }],
            options: {
                filter: 'roleGmecType == $rt',
                filterParams: { rt: "faculty" }
            },
            hidden: ({currentUser}) => {
                // can not edit
                return (currentUser.roles.find(({name}) => name === 'gmec')) 
            }
        },
        // required, Chief Academic Officer { reference, type: provider or person }
        // optional, Director of Medical Education { reference, type: provider or person }
        // optional, Administors Array [ of object { type: string (title), type: person }] NEVER a Healthcare Provider
        {
            name: 'program',
            title: 'Program(s)',
            description: `All programs for this GME Consortium.`,
            type: 'array',
            of: [
                { type: 'gmecProgram' },
            ],
        },
        {
            name: 'link',
            title: 'Link(s)',
            type: 'link',
        },
        /* TODO : Social Links */
        {
            name: 'descShort',
            title: 'short',
            type: 'string',
            description: 
                `Optional, short description.`,
            hidden: ({currentUser}) => {
                // can not edit
                return (currentUser.roles.find(({name}) => name === 'gmec')) 
            }
        },
        {
            name: 'descLong',
            title: 'long',
            type: 'locationPortableText',
            description:
                `Optional, long description.`,
            hidden: ({currentUser}) => {
                // can not edit
                return (currentUser.roles.find(({name}) => name === 'gmec')) 
            }
        },
        /* TODO : GME Consortium Assests, logos: Vertical, Horizontal and Square */
    ],
    orderings: [
        {
            name: 'nameAsc',
            title: 'Group Name a–>z',
            by: [
                {
                    field: 'name',
                    direction: 'asc'
                }
            ]
        },
        {
            name: 'nameDesc',
            title: 'Group Name z->a',
            by: [
                {
                    field: 'name',
                    direction: 'desc'
                }
            ]
        }
    ],
    preview: {
        select: {
            t: 'name',
            s: 'descShort',
            //i: 'locationImage',
        },
    prepare({ t, s }) {
            const title=`${t}`
            const subtitle=`${s}`
            return {
                /* media: i, */
                title: title,
                subtitle: subtitle
            }
        }
    }
}