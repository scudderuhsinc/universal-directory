import { RiGroup2Fill } from "react-icons/ri";

export default {
    name: 'membership',
    title: 'Membership',
    type: 'document',
    icon: RiGroup2Fill,
    fields: [
        {
            name: 'init',
            title: 'Initials',
            type: 'string'
        },
        {
            name: 'name',
            title: 'Name',
            type: 'string'
        },
        {
            name: 'descShort',
            title: 'Short Description',
            type: 'string',
        },
    ],
    orderings: [
        {
            name: 'initAsc',
            title: 'Credential a–>z',
            by: [
                {
                    field: 'init',
                    direction: 'asc'
                }
            ]
        },
        {
            name: 'initDesc',
            title: 'Credential z->a',
            by: [
                {
                    field: 'init',
                    direction: 'desc'
                }
            ]
        }
    ],
    preview: {
        select: {
            i: 'init',
            d: 'descShort',
        },
        prepare({ i, d }) {
            const t=`${i}`
            const st=`${d}`
            return {
                title: t,
                subtitle: st,
            }
        }
    }
}