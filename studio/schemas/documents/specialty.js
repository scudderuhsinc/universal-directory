import { RiAwardFill } from "react-icons/ri";

export default {
    name: 'specialty',
    title: 'Specialty Programs',
    type: 'document',
    icon: RiAwardFill,
    // used by Location and Provider documents
    fieldsets: [
        {
            name: 'descriptions',
            title: 'Description(s)',
            description: `Optional, specialty short and long descriptions.`,
            options: {
                collapsible: true,
                collapsed: true,
            }
        },
    ],
    fields: [
        {
            name: 'name',
            title: 'Name',
            type: 'string',
            description: `A name for a location's program / provider's treatment specialty.`,
            validation: Rule => Rule.error(`You have to define the specialties' name.`).required(),
        },
        {
            name: 'vertical',
            title: 'Healthcare Vertical',
            type: 'reference',
            validation: Rule => Rule.error(`You have to define the specialty's healthcare vertical.`).required(),
            to: {
                weak: true,
                type: 'vertical'
            }
        },
        {
            name: 'descShort',
            title: 'short',
            type: 'string',
            fieldset: 'descriptions',
        },
        {
            name: 'descLong',
            title: 'long',
            type: 'specialtyPortableText',
            fieldset: 'descriptions',
        },
    ],
    orderings: [
        {
            name: 'nameAsc',
            title: 'Location Name a–>z',
            by: [{
                    field: 'name',
                    direction: 'asc'
            }]
        },
        {
            name: 'nameDesc',
            title: 'Location Name z->a',
            by: [{
                    field: 'name',
                    direction: 'desc'
            }]
        }
    ],
    preview: {
        select: {
            t: 'name',
            s: 'descShort',
        },
        prepare({ t, s }){
            const title = `${t}`
            const subtitle = `${s}`
            return {
                title: title,
                subtitle: subtitle
            }
        }

    },
}