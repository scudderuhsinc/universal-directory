import { RiAccountCircleFill } from "react-icons/ri";

export default {
    name: 'provider',
    title: 'Providers',
    type: 'document',
    icon: RiAccountCircleFill,
    fields: [
        {
            name: 'nameFirst',
            title: 'First',
            type: 'string',
            validation: Rule => Rule.error(`You have to define the provider's first name.`).required(),
        },
        {
            name: 'nameMiddle',
            title: 'Middle (optional)',
            type: 'string',
            description: `leave off any periods (.)`,
        },
        {
            name: 'nameLast',
            title: 'Last',
            type: 'string',
            validation: Rule => Rule.error(`You have to define the provider's last name.`).required(),
        },
        {
            name: 'nameAka',
            title: 'Also Known As (optional)',
            type: 'string',
            description: `optional, this provider's a.k.a. or nickname. leave off any periods, slashes or special characters ( @ & # , + $ ~ % ' " : * ? ! () <> {} [])`,
        },
        {
            name: 'slug',
            type: 'slug',
            description: `url friendly string, 95 characters or less.`,
            options: {
                source: doc => doc.nameMiddle? `${doc.nameFirst}-${doc.nameMiddle}-${doc.nameLast}`:`${doc.nameFirst}-${doc.nameLast}`,
                maxLength: 95,
                slugify: input => input
                    .toLowerCase()
                    .replace(/[&\/\\#,+()$~%.'":*?<>{}]/g, '')
                    .replace(/ - /g, '-')
                    .replace(/\s+/g, '-')
                    .slice(0, 95)
            },
            hidden: ({currentUser}) => {
                // can not edit
                return (currentUser.roles.find(({name}) => name === 'gmec'))
            }
        },
        {
            name: 'profileImage',
            title: 'Profile Image',
            type: 'profileImage',
            hidden: ({currentUser}) => {
                // can not edit profile images
                return (currentUser.roles.find(({name}) => name === 'gmec' || name === 'gmec-manager')) 
            }
        },
        /* TODO: NPI (National Provider Identifier) 
        {
            name: 'npi',
            title: 'National Provider Identifier (NPI)',
            type: 'string',
            description: `An industry standard unique 10-digit identifier for individual healthcare providers or organizations, https://npiregistry.cms.hhs.gov`,
        }
        */
        {
            name: 'roleTypePrimary',
            title: 'Primary Role',
            type: 'document',
            description: `The provider's primary role or job title.`,
            weak: true,
            type: 'reference',
            // validation: Rule => Rule.error(`You have to define a primary role for this provider.`).required(),
            to: {
                type: 'role'
            },
            hidden: ({currentUser}) => {
                // can not edit
                return (currentUser.roles.find(({name}) => name === 'gmec'))
            }
        },

        /*
        {
            name: 'roleType',
            title: 'Role(s)',
            type: 'array',
            description: `Optional, any additional roles or job titles EXCLUDING any Graduate Medical Education Consortium related roles or titles.`,
            of: [
                weak: true,
                type: 'reference',
                to: {
                    type: 'role'
                },
            ],
        }
        */

        {
            name: 'roleTypeGmec',
            title: 'graduate medical education consortium',
            type: 'roleTypeGmec',
        },

        /*
        {
            name: 'specialty',
            title: 'Specialty(s)',
            type: 'array',
            validation: Rule => Rule.error(`You have to define at least one role for this provider.`).required(),
            of: [{
                weak: true,
                type: 'reference',
                to: {
                    type: 'specialty'
                }
            }],
        },
        */

        /*
        {
            name: 'location',
            title: 'Location(s)',
            description: `The provider's location or locations, 1st on the list is primary.`,
            type: 'array',
            icon: RiMapPinUserFill,
            validation: Rule => Rule.error(`You have to define at least one location for a provider.`).required(),
            of: [{
                weak: true,
                type: 'reference',
                to: {
                    type: 'location'
                }
            }],
        },
        */

        {
            name: 'credential',
            title: 'Credential(s)',
            description: `The provider's credentials for their role.`,
            type: 'array',
            of:[{
                weak: true,
                type: 'reference',
                to: {
                    type: 'credential'
                }
            }],
        },
        {
            name: 'education',
            title: 'Education',
            type: 'array',
            of: [
                { type: 'profileEdu'     },
                { type: 'profileEduGmec' },
            ],
        },

        /*
        {
            name: 'certification',
            title: 'Certification(s)',
            type: 'array',
            of: [
                { type: 'profileCertification' },
            ],
        },
        */
        {
            name: 'membership',
            title: 'Membership(s)',
            type: 'array',
            description:
                `for gmec see, GMEC Responsibility(s)`,
            of:[{
                weak: true,
                type: 'reference',
                to: {
                    type: 'membership'
                }
            }],
        },
        {
            name: 'biography',
            title: 'Biography',
            type: 'array',
            of: [
                { type: 'profileBio' },
            ],
        },
    ],
    orderings: [
        {
            name: 'lastAsc',
            title: 'Last Name a–>z',
            by: [
                {
                    field: 'nameLast',
                    direction: 'asc'
                }
            ]
        },
        {
            name: 'lastDesc',
            title: 'Last Name z->a',
            by: [
                {
                    field: 'nameLast',
                    direction: 'desc'
                }
            ]
        },
        {
            name: 'firstAsc',
            title: 'First Name a–>z',
            by: [
                {
                    field: 'nameFirst',
                    direction: 'asc'
                }
            ]
        },
        {
            name: 'firstDesc',
            title: 'First Name z->a',
            by: [
                {
                    field: 'nameFirst',
                    direction: 'desc'
                }
            ]
        }
    ],
    preview: {
        select: {
            f: 'nameFirst',
            l: 'nameLast',
            r: 'roleTypePrimary.name',
            i: 'profileImage',
        },
        prepare({ f, l, r, i }) {
            const title=`${f} ${l}`
            const subtitle=`${r}`
            return {
                media: i,
                title: title,
                subtitle: subtitle
            }
        }
    }
}
