import { RiShieldStarLine } from "react-icons/ri";

export default {
    name: 'vertical',
    title: 'Healthcare Verticals',
    type: 'document',
    icon: RiShieldStarLine,
    fieldsets: [
        {
            name: 'integrations',
            title: 'Service Integration(s)',
            description: `Optional, service integrations for this healthcare vertical.`,
            options: {
                collapsible: false,
            }
        },
    ],
    fields: [
        {
            name: 'name',
            title: 'Name',
            type: 'string',
            description: `Healthcare vertical name`, // add _ref ID
            validation: Rule => Rule.error(`A name is required for a healthcare vertical.`).required(),
        },
        {
            name: 'code',
            title: 'Code',
            type: 'slug',
            description: `Healthcare vertical code`,
            validation: Rule => Rule.error(`A code is required for a healthcare vertical.`).required(),
        },
        {
            name: 'callCenter',
            type: 'callCenter',
            fieldset: 'integrations'
        },
        {
            name: 'hln',
            title: 'Health Link Now',
            type: 'healthLinkNow',
            fieldset: 'integrations'
        },
    ],
    orderings: [
        {
            name: 'nameAsc',
            title: 'Location Name a–>z',
            by: [{
                    field: 'name',
                    direction: 'asc'
            }]
        }
    ],
    preview: {
        select: {
            n: 'name'
        },
        prepare({ n }) {
            const title=`${n}`
            return {
                title: title,
            }
        }
    }
}