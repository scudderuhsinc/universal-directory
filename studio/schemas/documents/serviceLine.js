import { RiEmpathizeFill } from "react-icons/ri";

export default {
    name: 'serviceLine',
    title: 'Service Lines',
    type: 'document',
    icon: RiEmpathizeFill,
    // used by Location documents
    fieldsets: [
        {
            name: 'descriptions',
            title: 'Description(s)',
            description: `Optional, service line short and long descriptions.`,
            options: {
                collapsible: true,
                collapsed: true,
            }
        },
    ],
    fields: [
        {
            name: 'name',
            title: 'Name',
            type: 'string',
            description: `service line name`, // add _ref ID
            validation: Rule => Rule.error(`You have to define a name for this service line.`).required(),
        },
        {
            name: 'vertical',
            title: 'Healthcare Vertical',
            type: 'reference',
            validation: Rule => Rule.error(`You have to define the service line's healthcare vertical.`).required(),
            to: {
                weak: true,
                type: 'vertical'
            }
        },
        {
            name: 'anchor',
            title: 'Anchor',
            description: `Location specific anchor text.`,
            validation: Rule => Rule.max(60).lowercase().warning(`must be lower case, contain no spaces and be no more than 60 characters.`),
            type: 'string',
        },
        {
            name: 'slug',
            type: 'string',
            description: `use "anchor" instead`,
        },
        {
            name: 'descShort',
            title: 'short',
            type: 'string',
            fieldset: 'descriptions',
        },
        {
            name: 'descLong',
            title: 'long',
            type: 'serviceLinePortableText',
            fieldset: 'descriptions',
        },
    ],
    orderings: [
        {
            name: 'nameAsc',
            title: 'Location Name a–>z',
            by: [{
                    field: 'name',
                    direction: 'asc'
            }]
        },
        {
            name: 'nameDesc',
            title: 'Location Name z->a',
            by: [{
                    field: 'name',
                    direction: 'desc'
            }]
        }
    ],
    preview: {
        select: {
            t: 'name',
            s: 'descShort',
        },
        prepare({ t, s }){
            const title = `${t}`
            const subtitle = `${s}`
            return {
                title: title,
                subtitle: subtitle
            }
        }

    },
}