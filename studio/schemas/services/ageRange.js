export default {
    name: 'ageRange',
    title: 'Age Range',
    type: 'object',
    fields: [
        {
            name: 'ageGroup',
            title: 'Age Group(s)',
            validation: Rule => Rule.unique().min(1).max(4).error(`You have to define at least one age group.`).required(),
            type: 'array',
            of: [{
                name: 'group',
                type: 'string',
                options: {
                    list: [
                        {title: 'Children', value: 'child'},
                        {title: 'Adolescents', value: 'adolescent'},
                        {title: 'Adults', value: 'adult'},
                        {title: 'Seniors', value: 'senior'},
                    ]
                },
            }]
        },
        {
            name: 'min',
            title: 'Minimum',
            description: `Optional, define the minimal age.`,
            type: 'number',
        },
        {
            name: 'max',
            title: 'Maximum',
            description: `Optional, define the maximum age.`,
            type: 'number',
        },
    ],
    preview: {
        select: {
            min: 'min',
            max: 'max',
        },
        prepare({ min, max }){
            const title = `Age Range`
            const subtitle = `${ Boolean(max) ? 'between: '+min+' - '+max+' yrs': 'minimum age: '+min}`
            return {
                title: title,
                subtitle: subtitle
            }
        }
    },
}