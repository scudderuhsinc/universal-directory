export default {
    name: 'treatmentService', 
    title: 'Treatment Service',
    type: 'object',
    fields: [
        {
            name: 'serviceLine',
            title: 'Service Line',
            validation: Rule => Rule.error(`You have to define a service line for this treatment service.`).required(),
            weak: true,
            type: 'reference',
            to: [{ type: 'serviceLine'}],
            options: {
                filter: ({document}) => {
                    if (!document.vertical) {
                        return // all service lines
                    }
                    return {
                        filter: 'references($v)',
                        params: {
                            v: document.vertical._ref
                        }
                    }
                }
            }
        },
        {
            name: 'levelCare',
            title: 'Level of Care',
            validation: Rule => Rule.error(`You have to define a level of care for this treatment service.`).required(),
            weak: true,
            type: 'reference',
            to: [{  type: 'levelCare' }],
            options: {
                filter: ({document}) => {
                    if (!document.vertical) {
                        return // all levels of care
                    }
                    return {
                        filter: 'references($v)',
                        params: {
                            v: document.vertical._ref
                        }
                    }
                }
            }
        },
        {
            name: 'diagnosis',
            title: 'Diagnoses',
            description: `Optional, if left undefined (wildcard) will cover all diagnoses under the defined Service Line.`,
            validation: Rule => Rule.warning(`Best practices require a dianosis for this treatment service.`),
            weak: true,
            type: 'reference',
            to: [{ type: 'diagnosis'}],
            options: {
                filter: ({document}) => {
                    if (!document.vertical) {
                        return // all diagnoses
                    }
                    return {
                        filter: 'references($v)',
                        params: {
                            v: document.vertical._ref
                        }
                    }
                }
            }
        },
        {
            name: 'treatment',
            title: 'Treatment',
            description: `Optional, if left undefined (wildcard) will cover all treatments under the defined Service Line.`,
            validation: Rule => Rule.warning(`Best practices require a treatment for this treatment service.`),
            weak: true,
            type: 'reference',
            to: [{ type: 'treatment' }],
            options: {
                filter: ({document}) => {
                    if (!document.vertical) {
                        return // all treatments
                    } 
                    return {
                        filter: 'references($v)',
                        params: {
                            v: document.vertical._ref
                        }
                    }
                }
            }
        },
        /* TODO : Support, Sex */
        {
            name: 'age',
            title: 'Age(s)',
            description: `All ages for this treament.`,
            type: 'ageRange',
        },
        /* TODO : add 'Deep Link' to a location's specific treatment service landing page. */
    ],
    preview: {
        select: {
            s: 'serviceLine.name',
            d: 'diagnosis.name',
            l: 'levelCare.name',
            t: 'treatment.name',
            min: 'age.min',
            max: 'age.max',
        },
        prepare({ s, d, l, t, min, max }) {
            const title = `${s}${ Boolean(d) ? ', '+d : ''}`
            const subtitle = `${l}${ Boolean(t) ? ' - '+t : ''}, ${ Boolean( min >= 0 ) ? 'ages: '+min : '' }${ Boolean( min == undefined && max != undefined ) ? 'ages: 0' : '' }${ Boolean( max >= 0 ) ? ' - '+max : '' }${ Boolean( min != undefined && max == undefined ) ? ' and older' : '' }${ Boolean( min == undefined && max == undefined ) ? 'ages: any' : '' }`
            return {
                title: title,
                subtitle: subtitle
            }
        }
    }
}