import ProgramInput from '../../components/gmecEduProgramInput'
import { RiMapPinUserFill } from "react-icons/ri";

export default {
    name: 'profileEduGmec',
    title: 'GMEC (UHS)',
    type: 'object',
    icon: RiMapPinUserFill,
    fields: [
        {
            name: 'gmec',
            title: 'UHS Graduate Medical Education Consortium',
            weak: true,
            type: 'reference',
            description: 'what GMEC is this Provider a graduate of?',
            to: {
                type: 'gmeConsortium',
            }
        },
        {
            name: 'program',
            title: 'Program',
            type: 'string',
            description: 'what Program is this Provider a graduate of?',
            inputComponent: ProgramInput
        },
        {
            name: 'type',
            type: 'string',
            description: 'what Type of Program was it?',
            options: {
                list: [
                    { title: 'Residency', value: 'residency' },
                    { title: 'Fellowship', value: 'fellowship' },
                ],
                layout: 'dropdown'
              }
        },
        {
            name: 'gradYear',
            title: 'Year',
            type: 'date',
            description: 'when did this Provider Graduate'
        }
    ],
    preview: {
        select: {
            n: 'gmec.name',
            p: 'program',
            t: 'type',
            y: 'gradYear'
        },
        prepare({ n, p, t, y }) {
            let title=`${n}`
            let subtitle=`not set`
            if ( t == "preliminary"){
                subtitle=`${p}, Preliminary Program - ${y} `
            }
            if ( t == "residency"){
                subtitle=`${p}, Residency Program - ${y}`
            }
            if ( t == "fellowship"){
                subtitle=`${p}, Fellowship Program - ${y}`
            }
            return {
                title: title,
                subtitle: subtitle
            }
        }  
    }
}