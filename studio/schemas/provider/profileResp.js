import { RiServiceFill } from "react-icons/ri";

export default {
    name: 'profileResp',
    title: 'Responsibility',
    type: 'object',
    icon: RiServiceFill,
    fields: [
        {
            name: 'name',
            title: 'Name',
            type: 'string',
        },
        {
            name: 'descShort',
            title: 'Short Description',
            type: 'string',
        },
    ],
    preview: {
        select: {
            n: 'name',
            d: 'descShort',
        },
        prepare({ n, d }) {
            let title=`${n}`
            let subtitle=`${d}`
            return {
                title: title,
                subtitle: subtitle
            }
        }  
    }
}