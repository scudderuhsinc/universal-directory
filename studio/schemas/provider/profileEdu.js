import { RiBankFill } from "react-icons/ri";

export default {
    name: 'profileEdu',
    title: 'Education',
    type: 'object',
    icon: RiBankFill,
    fields: [
        {
            name: 'type',
            title: 'Type',
            type: 'string',
            description:
                `education type, e.g. Medical School, Residency/Fellowship Program, Pharmacy School, pre-med ..`

        },
        {
            name: 'name',
            title: 'Name',
            type: 'string',
        },
        {
            name: 'location',
            title: 'Location',
            type: 'string',
        },
    ],
    preview: {
        select: {
            t: 'type',
            n: 'name',
            l: 'location',
            y: 'gradYear'
        },
        prepare({ t, n, l, y }) {
            let title=`${n}`
            let subtitle=`${t}, ${l}`
            return {
                title: title,
                subtitle: subtitle
            }
        }  
    }
}