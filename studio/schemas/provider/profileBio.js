import { RiMarkupFill } from "react-icons/ri";

export default {
    name: 'profileBio',
    title: 'Biography Block',
    type: 'object',
    icon: RiMarkupFill,
    fields: [
        {
            name: 'title',
            title: 'Title',
            description: 'block title: research, interests, hobbies ..',
            type: 'string',
        },
        {
            name: 'block',
            title: 'Name',
            type: 'profilePortableText',
        },
    ],
    preview: {
        select: {
            t: 'title',
        },
        prepare({ t }) {
            let title=`${t}`
            let subtitle=`Biography Block`
            return {
                title: title,
                subtitle: subtitle
            }
        }  
    }
}