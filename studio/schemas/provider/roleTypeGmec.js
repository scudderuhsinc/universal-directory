import ProgramInput from '../../components/gmecRoleProgramInput'
// TODO: import ProgramYearInput from '../../components/gmecProgramYearInput'

//console.log("type: "+document?.type)

export default {
    name: 'roleTypeGmec',
    type: 'object',
    fields: [
        {
            title: 'active',
            name: 'activeGmec',
            type: 'boolean'
        },
        {
            name: 'gmec',
            title: 'GMEC',
            weak: true,
            type: 'reference',
            to: {
                type: 'gmeConsortium',
            },
            hidden: ({parent}) => !parent?.activeGmec // hidden if not active with
        },
        {
            name: 'type',
            title: 'Type',
            type: 'string',
            options: {
                list: [
                    { title: 'Trainee', value: 'trainee' },
                    { title: 'Faculty', value: 'faculty' }
                ]
            },
            hidden: ({parent}) => !parent?.activeGmec // hidden if not active with
        },
        // TODO: Conditional Fields, where if "Trainee" show 'program', 'track' and 'year', else hide them.
        {
            name: 'program',
            title: 'Program',
            type: 'string',
            inputComponent: ProgramInput,
            /*
            validation: Rule => Rule.custom((gmecProgram, context) => {
                if ( context.parent.type == 'trainee' && gmecProgram == undefined ) {
                    return `You can't be a trainee AND NOT be associated with a Program.`
                }
                return true
            }),
            */
            hidden: ({parent}) => ( !parent?.activeGmec || parent?.type === 'faculty' ), // hidden if NOT active _OR_ IS faculty
        },
        // TODO :
        // async inputs, define Faculty roles for both Provider and GMEC documents
        // https://www.sanity.io/schemas/mutate-set-or-change-values-deep-within-documents-psPXQ26_wA_i
        {
            name: 'responsibility',
            title: 'GMEC Responsibility(s)',
            type: 'array',
            description:
                `for non-gmec see, Membership(s)`,
            of: [{
                    title: 'Membership',
                    weak: true,
                    type: 'reference',
                    to: {
                        type: 'membership'
                    }
                },
                {
                    type: 'profileResp'
                },
            ],
            hidden: ({parent}) => ( !parent?.activeGmec || parent?.type === 'trainee' ), // hidden if NOT active _OR_ IS trainee
        },
        {
            name: 'track',
            title: 'Track',
            type: 'string',
            options: {
                list: [
                    { title: 'Residency', value: 'residency' },
                    { title: 'Fellowship', value: 'fellowship' }
                ]
            },
            hidden: ({parent}) => ( !parent?.activeGmec || parent?.type === 'faculty' ), // hidden if NOT active _OR_ IS faculty
        },
        // TODO: Limit choices of year to gmec.program.duration and less
        {
            name: 'year',
            title: 'Year',
            type: 'string',
            // TODO: inputComponent: ProgramYearInput,
            /*
            validation: Rule => Rule.custom((year, context) => {
                if ( context.parent.type == 'trainee' && year == undefined ) {
                    return `You can't be a trainee AND NOT be associated with a Year.`
                }
                if ( context.parent.type == 'faculty' && year != undefined ) {
                    return `You can't be faculty member AND be associated with a year.`
                }
                return true
            }),
            */
            options: {
                list: [
                    { title: 'Preliminary | PGY-1',   value: 'ppgy-1' },
                    { title: 'Preliminary | PGY-2',   value: 'ppgy-2' },
                    { title: 'PGY-1 ( Categorical )', value: 'cpgy-1' },
                    { title: 'PGY-2 ( Categorical )', value: 'cpgy-2' },
                    { title: 'PGY-3 ( Categorical )', value: 'cpgy-3' },
                    { title: 'PGY-4 ( Categorical )', value: 'cpgy-4' },
                    { title: 'PGY-5 ( Categorical )', value: 'cpgy-5' },
                    { title: 'PGY-6 ( Categorical )', value: 'cpgy-6' },
                ]
            },
            hidden: ({parent}) => ( !parent?.activeGmec || parent?.type === 'faculty' ), // hidden if NOT active _OR_ IS faculty
        }
    ]
}