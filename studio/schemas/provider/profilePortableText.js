export default {
    name: 'profilePortableText',
    title: 'Profile Description',
    type: 'array',
    of: [{
        type: 'block',
        title: 'Copy Block',
        styles: [
            { title: 'Normal', value: 'normal' },
            { title: 'Quote', value: 'blockquote' }
        ],
        lists: [{ title: 'Bullet', value: 'bullet' }, { title: 'Numbered', value: 'number' }],
        marks: {
            decorators: [{ title: 'Strong', value: 'strong' }, { title: 'Emphasis', value: 'em' }],
            // Annotations can be any object structure – e.g. a link or a footnote.
        },
    }]
}