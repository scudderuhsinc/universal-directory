import createSchema from 'part:@sanity/base/schema-creator'
import schemaTypes from 'all:part:@sanity/base/schema-type'

// documents
import credential from './documents/credential'
import diagnosis from './documents/diagnosis'
import gmeConsortium from './documents/gmeConsortium'
import levelCare from './documents/levelCare'
import location from './documents/location'
import locationGrp from './documents/locationGrp'
import membership from './documents/membership'
import provider from './documents/provider'
import role from './documents/role'
import serviceLine from './documents/serviceLine'
import specialty from './documents/specialty'
import treatment from './documents/treatment'
import vertical from './documents/vertical'

// objects
import galleryImage from './objects/galleryImage'
import link from './objects/link'

// integration objects
import callCenter from './integrations/callCenter'
import formCarry from './integrations/formCarry'
import formLabelInput from './integrations/formLabelInput'
import healthLinkNow from './integrations/healthLinkNow'
import hlnServiceArea from './integrations/hlnServiceArea'

// location objects
import close from './location/close'
import gmecBlock from './location/gmecBlock'
import gmecBlockLocation from './location/gmecBlockLocation'
import gmecProgram from './location/gmecProgram'
import hrs from './location/hrs'
import locationPortableText from './location/locationPortableText'
import open from './location/open'
import telNum from './location/telNum'
import usAddr from './location/usAddr'

// provider objects
import profileBio from './provider/profileBio'
import profileEdu from './provider/profileEdu'
import profileEduGmec from './provider/profileEduGmec'
import profileImage from './provider/profileImage'
import profilePortableText from './provider/profilePortableText'
import profileResp from './provider/profileResp'
import roleTypeGmec from './provider/roleTypeGmec'

// service objects
import ageRange from './services/ageRange'
import serviceLinePortableText from './services/serviceLinePortableText'
import specialtyPortableText from './services/specialtyPortableText'
import treatmentPortableText from './services/treatmentPortableText'
import treatmentService from './services/treatmentService'

export default createSchema({
  name: 'default',
  types: schemaTypes.concat([
    // objects
    ageRange,
    callCenter,
    close,
    formCarry,
    formLabelInput,
    galleryImage,
    gmecBlock,
    gmecBlockLocation,
    gmecProgram,
    healthLinkNow,
    hlnServiceArea,
    hrs,
    locationPortableText,
    link,
    open,
    profileBio,
    profileEdu,
    profileEduGmec,
    profileImage,
    profilePortableText,
    profileResp,
    roleTypeGmec,
    serviceLinePortableText,
    specialtyPortableText,
    telNum,
    treatmentPortableText,
    treatmentService,
    usAddr,
    // documents
    provider,
    role,
    credential,
    membership,
    location,
    locationGrp,
    gmeConsortium,
    serviceLine,
    diagnosis,
    levelCare,
    treatment,
    specialty,
    vertical
  ]),
})
